﻿using System;
using System.Runtime.InteropServices;
using WorkSpaceInjector.Enums;

namespace WorkSpaceInjector
{
    public class Win32Api
    {
        [DllImport("user32.dll")]
        public static extern bool SetWindowDisplayAffinity(IntPtr hwnd, DisplayAffinity affinity);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr parentWindow, IntPtr previousChildWindow, string windowClass, string windowTitle);

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr window, out int process);
    }
}
