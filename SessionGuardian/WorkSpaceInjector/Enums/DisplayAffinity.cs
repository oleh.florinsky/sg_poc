﻿namespace WorkSpaceInjector.Enums
{
    public enum DisplayAffinity : uint
    {
        None = 0,
        Monitor = 1
    }
}
