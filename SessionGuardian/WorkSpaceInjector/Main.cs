﻿namespace WorkSpaceInjector
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    using WorkSpaceInjector.Enums;

    public class Main
    {
        private static Timer _sendKeepTimer;
        public void Run()
        {

            try
            {
                new Thread(() =>
                {
                    _sendKeepTimer = new System.Threading.Timer((x) => BlackingWindow(x, Process.GetCurrentProcess().Id), null, 2000, Timeout.Infinite);
                }).Start();
                //var check2 = Directory.GetCurrentDirectory() + @"\LogInfo\SG_APP_LOG.log";
                //File.AppendAllText(check2, $"\r\nMessage from injected DLL -------------------------------------------------------Cur dir: {Process.GetCurrentProcess().Id} " + Directory.GetCurrentDirectory());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private  void BlackingWindow(Object state, int id)
        {
            var windows = GetProcessWindows(id);

            if (windows.Length > 0)
            {
                foreach (var item in windows)
                {
                    bool result = Win32Api.SetWindowDisplayAffinity(item, DisplayAffinity.Monitor);
                }
            }
            _sendKeepTimer.Change(2000, Timeout.Infinite);
        }

        private IntPtr[] GetProcessWindows(Int32 pid)
        {

            var apRet = new List<IntPtr>();
            var pLast = IntPtr.Zero;
            var currentPid = 0;

            do
            {
                pLast = Win32Api.FindWindowEx(IntPtr.Zero, pLast, null, null);
                Win32Api.GetWindowThreadProcessId(pLast, out currentPid);

                if (currentPid == pid)
                    apRet.Add(pLast);

            } while (pLast != IntPtr.Zero);

            return apRet.ToArray();
        }
    }
}
