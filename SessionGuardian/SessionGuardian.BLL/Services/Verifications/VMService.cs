﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.Profile;
    using System.Management;
    using System.Threading;
    using System.Threading.Tasks;

    public class VMService : BaseVerification
    {

        public async override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {

            if (successInviroments)
            {
                var isVirtualMachine = DetectVirtualMachine();
                actualState = ProfileService.GetInstance.Configuration.AllowVM ? true : !isVirtualMachine;
            }
            else
            {
                actualState = true;
            }
        }


        public bool DetectVirtualMachine()
        {
            bool result = false;
            try
            {
                const string MICROSOFTCORPORATION = "microsoft corporation";
                using (var searcher = new System.Management.ManagementObjectSearcher("Select * from Win32_ComputerSystem"))
                {
                    using (var items = searcher.Get())
                    {
                        foreach (var item in items)
                        {
                            string manufacturer = item["Manufacturer"].ToString().ToLower();
                            var value1 = item["Model"].ToString().ToUpperInvariant().Contains("VIRTUAL");
                            var value2 = manufacturer.Contains("vmware");
                            var value3 = item["Model"].ToString() == "VirtualBox";
                            var value4 = manufacturer.Contains("parallels");

                            LogService.Info("----------------------------VM information-----------------------", false, nameof(VMService));
                            LogService.Info($"Manufacturer is: \"{manufacturer}\"", false, nameof(VMService));
                            LogService.Info($"Is contains \"VIRTUAL\": {value1}", false, nameof(VMService));
                            LogService.Info($"Is contains \"vmware\": {value2}", false, nameof(VMService));
                            LogService.Info($"Is contains \"parallels\": {value4}", false, nameof(VMService));
                            LogService.Info($"Is contains \"VirtualBox\": {value3}", false, nameof(VMService));
                            LogService.Info("--------------------------------------------------------------------", false, nameof(VMService));

                            if ((manufacturer == MICROSOFTCORPORATION && item["Model"].ToString().ToUpperInvariant().Contains("VIRTUAL"))
                            || manufacturer.Contains("vmware") || manufacturer.Contains("parallels")
                            || item["Model"].ToString() == "VirtualBox")
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (ManagementException ex)
            {
                LogService.Info(ex.StackTrace);
                return result;
            }
        }
    }
}
