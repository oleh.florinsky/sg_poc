﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Guardians;
    using System.Threading;
    using System.Threading.Tasks;

    public class CameraService : BaseVerification
    {
        public async override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            actualState = CamDeviceService.HasConnectedDevice();
        }
    }
}
