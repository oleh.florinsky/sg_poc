﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Guardians;
    using SessionGuardian.BLL.Services.Profile;
    using System.Threading;
    using System.Threading.Tasks;

    public class SGSecurityService : BaseVerification
    {
        public async override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            if (successInviroments)
            {
                var processService = ProcessService.GetInstance;
                var forbiddenList = ProfileService.GetInstance.Configuration.ForbiddenApps;
                actualState = processService.CheckStartedForbiddenProcesses(forbiddenList);
            }
            else
            {
                actualState = true;
            }
        }
    }
}
