﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Models.Antivirus;
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Profile;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class AntivirusService : BaseVerification
    {
        public  async override Task  UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            if (ProfileService.GetInstance.Configuration != null)
            {
                actualState = ProfileService.GetInstance.Configuration.AvCheck ? GetAntivirusProductsState() : true;
            }
            else
            {
                actualState = GetAntivirusProductsState();
            }
        }

        public static bool GetAntivirusProductsState()
        {

            List<AntiVirusWmiDto> antivirusList;

            using (var wmi = new WmiService<AntiVirusWmiDto>(new AntiVirusWmiDto(), "root\\SecurityCenter2"))
            {
                antivirusList = wmi.GetObjectList();
            }

            var aVStateInRealTime = GetRealTimeScannerState(antivirusList);

            return (antivirusList.Count > 0);



        }

        public static bool GetRealTimeScannerState(List<AntiVirusWmiDto> antivirusList)
        {
            var avInventoryList = new List<AntiVirusInventoryDto>();

            foreach (var a in antivirusList)
            {
                var avInventory = new AntiVirusInventoryDto();

                string provider;
                string realTimeScanner;
                string upToDate;

                try
                {
                    avInventory.DisplayName = a.DisplayName;
                    var hex = a.ProductState.ToString("X6");
                    var productState = a.ProductState.ToString();
                    avInventory.ProductState = Convert.ToInt32(productState);
                    provider = hex.Substring(0, 2);
                    realTimeScanner = hex.Substring(2, 2);
                    upToDate = hex.Substring(4, 2);
                }
                catch
                {
                    //ignored
                    continue;
                }


                switch (provider)
                {
                    case "05":
                    case "07":
                        avInventory.Provider = "AntiVirus-Firewall";
                        break;
                    case "04":
                    case "06":
                        avInventory.Provider = "AntiVirus";
                        break;
                    default:
                        avInventory.Provider = "Unknown";
                        break;
                }

                switch (realTimeScanner)
                {
                    case "00":
                        avInventory.RealtimeScanner = "Off";
                        break;
                    case "01":
                        avInventory.RealtimeScanner = "Expired";
                        break;
                    case "10":
                        avInventory.RealtimeScanner = "On";
                        break;
                    case "11":
                        avInventory.RealtimeScanner = "Snoozed";
                        break;
                    default:
                        avInventory.Provider = "Unknown";
                        break;
                }

                switch (upToDate)
                {
                    case "00":
                        avInventory.DefinitionStatus = "Up To Date";
                        break;
                    case "10":
                        avInventory.DefinitionStatus = "Out Of Date";
                        break;
                    default:
                        avInventory.DefinitionStatus = "Unknown";
                        break;
                }
                avInventoryList.Add(avInventory);
            }

            var activeInRealTimeAntivirusList = avInventoryList.Where(x => x.RealtimeScanner == "On");
            return activeInRealTimeAntivirusList.Count() > 0 ? true : false;
        }
    }
}
