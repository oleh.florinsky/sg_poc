﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using Newtonsoft.Json;
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Models.Http;
    using SessionGuardian.BLL.Models.Sockets;
    using SessionGuardian.BLL.Models.Sockets.Request;
    using SessionGuardian.BLL.Models.Sockets.Responce;
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Models.Workspace;
    using SessionGuardian.BLL.Services.Error;
    using SessionGuardian.BLL.Services.Secure;
    using SessionGuardian.BLL.Services.WebSockets;
    using SessionGuardian.Common.Enums.WebSockets;
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.WebSockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class SRServerService : BaseVerification
    {
        AppConfiguration appConfiguration;

        public static WebSocketService webSocketService;
        HttpClient client;
        public SRServerService()
        {
            webSocketService = WebSocketService.GetInstance;
        }

        public async override Task UpdateActualState(bool successInviroments, CancellationToken token = default)
        {
            if (successInviroments)
            {
                var configuration = CredentialService.GetUserConfiguration();
                appConfiguration = AppConfiguration.GetInstance;

                if (appConfiguration.SgToken == null)
                {
                    // allow HTTPS connection
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    var link = configuration.SgEndpoint;
                    client = new HttpClient();
                    client.BaseAddress = new Uri("https://" + link);

                    var data = new ClientRegisterRequest
                    {
                        Token = configuration.JwtToken
                    };
                    var res = await client.PostAsync("api/register", new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
                    if (res.StatusCode == HttpStatusCode.BadRequest) // false if jwt Token is expired
                    {
                        ErrorService.GetInstance.SendNotificationMessage("JwtToken expired. Please contact support to recive a registration link.");

                        actualState = false;
                        return;
                    }
                    var jsonData = await res.Content.ReadAsStringAsync();
                    var typedresponse = JsonConvert.DeserializeObject<ClientRegisterResponse>(jsonData);
                    appConfiguration.SetConfiguration(typedresponse);
                }

                if (webSocketService.Client.State == WebSocketState.Closed || webSocketService.Client.State == WebSocketState.None)
                {
                    await webSocketService.Connect(configuration.SgEndpoint + appConfiguration.WsConnectionUri, appConfiguration.SgToken);
                    WorkspaceModel response = await webSocketService.SendMessage<WorkspaceModel>(SocketMessageType.GET_WORKSPACE_DATA, "workspace", token, SocketMessageType.GET_WORKSPACE_DATA_SUCCESS);
                    if (response != null)
                    {
                        appConfiguration.RegistrationCode = response.RegistrationCode;
                        appConfiguration.UserName = response.UserName;
                        actualState = true;
                    }
                    else
                    {
                        actualState = false;
                    }
                }
                else
                {
                    actualState = true;
                }
            }
            else
            {
                actualState = false;
            }
        }
    }
}
