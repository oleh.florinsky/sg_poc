﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Interfaces;
    using SessionGuardian.BLL.Models.Verification;
    using System;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using Luxand;
    using System.Reflection;
    using System.IO;
    using System.Runtime.InteropServices;


    public class InternetService : BaseVerification
    {
      
        public InternetService()
        {
            //int state = FSDKCam.InitializeCapturing();
            //int sdk = FSDK.ActivateLibrary("vKH7eS8BnDHySF7/JnAuIL2ntNetFq2BAPHcCXMlL4JMTxMNEfrK7fKKGR7yDnYOSaEE25LKjhHQmr2l/MN2L3/LSyOtd3tNrnN65hnWsMQawzEBZrYMMWT8weaAt6zlvKeIS+D6CVh0S5bPB+xZUx4ylUYSKMd53jGMXUqKIoA=");
            //FSDK.InitializeLibrary();
            //var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\Resource\\photo.png";
            //var path1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\Resource\\photo1.png";


            //int image = 0;
            //int image1 = 0;

            //FSDK.LoadImageFromFile(ref image, path);
            //FSDK.LoadImageFromFile(ref image1, path1);




            //TFacePosition position = new TFacePosition();
            //var start = DateTime.Now;



            //var res = FSDK.DetectFace(image, ref position);
            //int different = (int)(DateTime.Now - start).TotalMilliseconds;

            //var start1 = DateTime.Now;
            //byte[] imageByte;
            //var result5 = FSDK.GetFaceTemplate(image, out imageByte);
            //int different1 = (int)(DateTime.Now - start1).TotalMilliseconds;
            //var time1 = different1 + "ms";

            //var start2 = DateTime.Now;
            //byte[] imageByte2;
            //var result6 = FSDK.GetFaceTemplateInRegion(image, ref position, out imageByte2);


            //float similarity = 0;
            //var compare = FSDK.MatchFaces(ref imageByte2, ref imageByte2, ref similarity);
            //int different2 = (int)(DateTime.Now - start1).TotalMilliseconds;
            //var time2 = different2 + "ms";


            //FSDK.FinalizeLibrary();
        }
        //public volatile bool actualState = false;


        public async override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            var network = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                using (WebClient webClient = new WebClient())
                {
                    // allow HTTPS connection
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    webClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
                    webClient.Proxy = null;
                    webClient.OpenReadCompleted += webClient_OpenReadCompleted;
                    webClient.OpenReadAsync(new Uri("http://google.com/generate_204"));
                }
            }

            while (!actualState)
            {
                if (token.IsCancellationRequested)
                {
                    actualState = false;
                    break;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }


        void webClient_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                actualState = true;
            }
            else
            {
                actualState = false;
            }
        }
    }
}
