﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using Newtonsoft.Json;
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models;
    using SessionGuardian.BLL.Models.AWS;
    using SessionGuardian.BLL.Models.Sockets;
    using SessionGuardian.BLL.Models.Sockets.Responce;
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Models.WaterMark;
    using SessionGuardian.BLL.Services.AWS;
    using SessionGuardian.BLL.Services.Profile;
    using SessionGuardian.BLL.Services.WaterMark;
    using SessionGuardian.BLL.Services.WebSockets;
    using SessionGuardian.Common.Enums.WebSockets;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class UserProfileService : BaseVerification
    {
        private WebSocketService webSocketService;


        public UserProfileService()
        {
            webSocketService = WebSocketService.GetInstance;
        }

        public async override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            Console.WriteLine($"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ => Start verify the Profile data");

            var profileState = await GetProfileConfigState(token);

            var toeknState = await GetAWSCrdentialsState(token);

            var waterMarkState = await GetWaterMarkConfigState(token);

            actualState = profileState && toeknState && waterMarkState ? true : false;

        }


        private async Task<bool> GetProfileConfigState(CancellationToken token)
        {
            var response = await webSocketService.SendMessage<ProfileConfigModel>(SocketMessageType.GET_PROFILE_CONFIG, "profile", token, SocketMessageType.GET_PROFILE_CONFIG_SUCCESS);
            if (response != null)
            {
                ProfileService.GetInstance.SetProfileConfiguration(response);
                return true;
            }
            else
            {
                return false;
            }




            //var profileMessage = new RequestMessageModel()
            //{
            //    Type = SocketMessageType.GET_PROFILE_CONFIG.ToString(),
            //    Payload = null
            //};
            //var profileRequest = webSocketService.CreateRequestMessage(profileMessage, "profile");
            //webSocketService.SendMessage(SocketRequestType.SendMessage, profileRequest, SocketMessageType.GET_PROFILE_CONFIG_SUCCESS);

            //ResponseMessageModel response;
            //while (!webSocketService.TryGetReceivedMessage(out response))
            //{
            //    Task.Delay(100);
            //}
            //return JsonConvert.DeserializeObject<ProfileConfigModel>(response.Payload.ToString());
        }



        private async Task<bool> GetAWSCrdentialsState(CancellationToken token)
        {
            var response = await webSocketService.SendMessage<AWSCredentialsModel>(SocketMessageType.GET_SECURITY_TOKENS, "awsToken", token, SocketMessageType.GET_SECURITY_TOKENS_SUCCESS);
            if (response != null)
            {
                var awsCredentialService = AWSCredentialService.GetInstance;
                awsCredentialService.SetCredentials(response);
                awsCredentialService.StartExpiredTimer();
                return true;
            }
            else
            {
                return false;
            }



            //var awsTokenMessage = new RequestMessageModel()
            //{
            //    Type = SocketMessageType.GET_SECURITY_TOKENS.ToString(),
            //    Payload = null
            //};

            //var awsTokenRequest = webSocketService.CreateRequestMessage(awsTokenMessage, "awsToken");
            //webSocketService.SendMessage(SocketRequestType.SendMessage, awsTokenRequest, SocketMessageType.GET_SECURITY_TOKENS_SUCCESS);


            //ResponseMessageModel response;
            //while (!webSocketService.TryGetReceivedMessage(out response))
            //{
            //    Task.Delay(100);
            //}


            //return JsonConvert.DeserializeObject<AWSCredentialsModel>(response.Payload.ToString());
        }


        private async Task<bool> GetWaterMarkConfigState(CancellationToken token)
        {
            var response = await webSocketService.SendMessage<WaterMarkConfigModel>(SocketMessageType.GET_WATERMARK_CONFIG, "waterMark", token, SocketMessageType.GET_WATERMARK_CONFIG_SUCCESS);
            if (response != null)
            {
                var waterMarkService = WaterMarkService.GetInstance;
                waterMarkService.SetConfiguration(response);
                return true;
            }
            else
            {
                return false;
            }
            //var waterMarkMessage = new RequestMessageModel()
            //{
            //    Type = SocketMessageType.GET_WATERMARK_CONFIG.ToString(),
            //    Payload = null
            //};

            //var awsTokenRequest = webSocketService.CreateRequestMessage(waterMarkMessage, "waterMark");
            //webSocketService.SendMessage(SocketRequestType.SendMessage, awsTokenRequest, SocketMessageType.GET_WATERMARK_CONFIG_SUCCESS);

            //ResponseMessageModel response;
            //while (!webSocketService.TryGetReceivedMessage(out response))
            //{
            //    Task.Delay(100);
            //}

            //return JsonConvert.DeserializeObject<WaterMarkConfigModel>(response.Payload.ToString());
        }
    }
}
