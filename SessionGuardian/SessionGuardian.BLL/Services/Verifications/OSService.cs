﻿namespace SessionGuardian.BLL.Services.Verifications
{
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Profile;
    using System.Threading;
    using System.Threading.Tasks;

    public class OSService : BaseVerification
    {
        public async  override Task UpdateActualState(bool successInviroments = true, CancellationToken token = default)
        {
            if (ProfileService.GetInstance.Configuration != null)
            {
                actualState = ProfileService.GetInstance.Configuration.OsCheck ? true : true;
            }
            else
            {
                actualState = true;
            }

        }
    }
}
