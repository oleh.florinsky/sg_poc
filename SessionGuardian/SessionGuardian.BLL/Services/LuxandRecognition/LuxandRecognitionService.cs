﻿namespace SessionGuardian.BLL.Services.LuxandRecognition
{
    using Luxand;
    using SessionGuardian.BLL.Interfaces.Luxand;
    using SessionGuardian.BLL.Models;
    using SessionGuardian.BLL.Models.Recognition;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.Profile;
    using SessionGuardian.BLL.Services.Recognition;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public sealed class LuxandRecognitionService : BaseRecognitionService, ILuxandRecognitionInterface
    {

        private static readonly object Instancelock = new object();
        private static LuxandRecognitionService instance = null;
        public static LuxandRecognitionService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new LuxandRecognitionService();
                        }
                    }
                }
                return instance;
            }
        }
        List<long> filteredIds;

        Image i;
        /// <summary>
        /// A verification threshold
        /// </summary>
        double verificationThreshold = (double)ProfileService.GetInstance.Configuration.FaceMatchConfidence / 100;

        /// <summary>
        /// An internal resize width parameter. It determines quality of recognition. 
        /// </summary>
        int internalWidth = 100;

        bool AnchorHasOpenEyes;

        /// <summary>
        /// A threhold for a face detection
        /// </summary>
        int detectionThreshold = 4;

        /// <summary>
        /// A string that contains a tracker configuration
        /// </summary>
        string trackerConfig;

        /// <summary>
        /// A tracker handler
        /// </summary>
        int trackerHandler = 0;

        /// <summary>
        /// An error handler
        /// </summary>
        int errorHandler = 0;

        /// <summary>
        /// An id of an anchor person
        /// </summary>
        long anchorID = 0;

        /// <summary>
        /// A minimal required square of a face relatively a camera image
        /// </summary>
        double minimalRelativeSquare = 0.01;

        long[] ids;

        System.Timers.Timer timer;
        long faceCount = 0;

        /// <summary>
        /// A license key
        /// </summary>
        String key = "vKH7eS8BnDHySF7/JnAuIL2ntNetFq2BAPHcCXMlL4JMTxMNEfrK7fKKGR7yDnYOSaEE25LKjhHQmr2l/MN2L3/LSyOtd3tNrnN65hnWsMQawzEBZrYMMWT8weaAt6zlvKeIS+D6CVh0S5bPB+xZUx4ylUYSKMd53jGMXUqKIoA=";






        private int anchorImageHandler = 0;
        string anchorPath = Path.GetDirectoryName(typeof(LuxandRecognitionService).Assembly.Location) + $"\\Resource\\Photos\\face1.png";

        public LuxandRecognitionService()
        {
            timer = new System.Timers.Timer()
            {
                AutoReset = false
            };
            timer.Elapsed += (o, args) =>
            {
                timer.Stop();
                AnchorHasOpenEyes = false;
            };
            timer.Interval = ProfileService.GetInstance.Configuration.ConsecutiveCycles * 1000;
            //timer.Enabled = true;


            trackerConfig = $"DetectExpression=true; Threshold={verificationThreshold}; HandleArbitraryRotations=true; DetermineFaceRotationAngle=false; InternalResizeWidth={internalWidth}; FaceDetectionThreshold={detectionThreshold};";
            if (FSDK.FSDKE_OK != FSDK.ActivateLibrary(key))
            {
                MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)", "Error activating FaceSDK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            FSDK.InitializeLibrary();
            LogService.Info($"Patch to achor: {anchorPath}", false, nameof(LuxandRecognitionService));
            //Load an anchor image
            if (FSDK.FSDKE_OK != FSDK.LoadImageFromFileW(ref anchorImageHandler, anchorPath))
            {
                MessageBox.Show("Please select a propper anchor image", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            FSDK.CreateTracker(ref trackerHandler);

            errorHandler = 0;
            FSDK.SetTrackerMultipleParameters(trackerHandler, trackerConfig, ref errorHandler);

            long[] ids = { };
            faceCount = 0;

            //Add timeout
            while (faceCount != 1)
            {
                FSDK.FeedFrame(trackerHandler, 1, anchorImageHandler, ref faceCount, out ids, sizeof(long) * 256);
            }
            Console.WriteLine($"New frame {faceCount}_ {Thread.CurrentThread.ManagedThreadId}");
            Array.Resize(ref ids, (int)faceCount);
            anchorID = ids[0];
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public CompareFacesOutputModel FindAnchorFace()
        {
            try
            {
                String names;
                FSDK.GetIDReassignment(trackerHandler, anchorID, ref anchorID);
                FSDK.GetAllNames(trackerHandler, anchorID, out names, sizeof(long) * 100);

                bool isAnchorId = filteredIds.Count > 0 ? filteredIds.ElementAt(0) == anchorID : false;
                Console.WriteLine($"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Has target photo: {isAnchorId}");

                String faceAttributes;
                FSDK.GetTrackerFacialAttribute(trackerHandler, 0, filteredIds.ElementAt(0), "Expression", out faceAttributes, sizeof(long) * 256);

                //Extract eyes
                float confidence = 0f;
                FSDK.GetValueConfidence(faceAttributes, "EyesOpen", ref confidence);
                Console.WriteLine("Eyes: confidence: " + confidence);
                if (confidence > 0.65)
                {
                    Console.WriteLine("++++++Open eyes");
                    AnchorHasOpenEyes = true;
                    if (timer.Enabled)
                    {
                        timer.Stop();
                    }
                }
                else
                {
                    Console.WriteLine("-------Close eyes");
                    if (!timer.Enabled)
                    {
                        timer.Start();
                    }
                }

                var compareResult = new CompareFacesOutputModel()
                {
                    HasTargetFace = isAnchorId,
                    OpenEyes = AnchorHasOpenEyes,
                };
                return compareResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public FaceDetectOutputModel DetectFaceAsync(IntPtr handle)
        {

            try
            {
                var image = new FSDK.CImage(handle);
                FSDK.FeedFrame(trackerHandler, 0, image.ImageHandle, ref faceCount, out ids, sizeof(long) * 100); // maximum of 256 faces detected

                Array.Resize(ref ids, (int)faceCount);
                filteredIds = filterDetectedFaces(ids, trackerHandler, 0);
                Console.WriteLine($"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++New frame {faceCount}_ {filteredIds.Count}");
                //imageBitmap.Dispose();
                //cam.PlayingFinished += Cam_PlayingFinished;


                return new FaceDetectOutputModel()
                {
                    Count = (int)faceCount
                };
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public async Task<LabelDetectOutputModel> DetectLabelsAsync(Bitmap image)
        {

            throw new System.NotImplementedException();
        }

        public List<long> filterDetectedFaces(long[] ids, int tracker, int cameraIdx)
        {
            List<long> list = new List<long>();

            foreach (long id in ids)
            {
                list.Add(id);
                FSDK.TFacePosition position = new FSDK.TFacePosition();
                FSDK.GetTrackerFacePosition(tracker, cameraIdx, id, ref position);
                if ((position.w * position.w) / (640 * 480) > minimalRelativeSquare) list.Add(id);
            }

            return list;
        }


    }
}
