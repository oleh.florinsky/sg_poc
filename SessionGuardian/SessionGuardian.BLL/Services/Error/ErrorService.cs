﻿namespace SessionGuardian.BLL.Services.Error
{
    public sealed class ErrorService
    {
        private static readonly object Instancelock = new object();
        private static ErrorService instance = null;

        public delegate void CriticalNotification(string message = "", bool showPopup = true);
        public event CriticalNotification CriticalNotificationHandler;

        public static ErrorService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new ErrorService();
                        }
                    }
                }
                return instance;
            }
        }

        public void  SendNotificationMessage(string message)
        {
            CriticalNotificationHandler.Invoke(message);
        }

        public void StopSecureSession()
        {
            CriticalNotificationHandler.Invoke(showPopup: false);
        }
    }
}
