﻿namespace SessionGuardian.BLL.Services.WaterMark
{
    using SessionGuardian.BLL.Models.WaterMark;

    public sealed class WaterMarkService
    {
        private static readonly object Instancelock = new object();
        private static WaterMarkService instance = null;

        public static WaterMarkService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new WaterMarkService();
                        }
                    }
                }
                return instance;
            }
        }
        public WaterMarkConfigModel Configurations { get; set; }

        public void SetConfiguration(WaterMarkConfigModel configurations)
        {
            Configurations = configurations;
        }

    }
}
