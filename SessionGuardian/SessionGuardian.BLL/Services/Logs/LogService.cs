﻿
using log4net;
using SessionGuardian.BLL.Helpers.Logs;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace SessionGuardian.BLL.Services.Logs

{
    public class LogService
    {
        private static readonly ILog customLog = LogHelper.GetLoggerRollingFileAppender(nameof(LogService), Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\logInfo\\SG_APP_LOG.log");


        public static void InfoStart()
        {
            var startMessage = "============================= START APPLICATION =============================";

            customLog.Info("");
            customLog.Info("");
            customLog.Info("=============================================================================");
            customLog.Info(startMessage);
            customLog.Info("=============================================================================");

            SendMessageToEventLog(startMessage);
        }

        public static void Info(string text, bool sendToEventViever = false, string className = "")
        {
            var message = $"({ className}) => {text}";
            customLog.Info(message);

            if (sendToEventViever)
            {
                SendMessageToEventLog(message);
            }
        }

        public static void Error(string text, bool sendToEventViever = false, string className = "")
        {
            var message = $"({className}) => {text}";
            customLog.Error(message);
            if (sendToEventViever)
            {
                SendMessageToEventLog(message);
            }
        }

        public static void SendMessageToEventLog(string message)
        {
            if (!EventLog.SourceExists("SessionGuardionSource"))
            {
                EventLog.CreateEventSource("SessionGuardionSource", "DevSessionGuardionSource");
            }
            var devLog = new EventLog();
            devLog.Source = "SessionGuardionSource";
            devLog.WriteEntry(message);
        }
    }
}
