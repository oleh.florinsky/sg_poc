﻿namespace SessionGuardian.BLL.Services.Profile
{
    using SessionGuardian.BLL.Models;
    using SessionGuardian.BLL.Services.Error;

    public sealed class ProfileService
    {
        private static readonly object Instancelock = new object();


        public ProfileConfigModel Configuration { get; set; }

        private static ProfileService instance = null;


        public static ProfileService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new ProfileService();
                        }

                    }
                }
                return instance;
            }
        }



        public void SetProfileConfiguration(ProfileConfigModel profileconfig)
        {
            bool isValid = CheckProfileValidation(profileconfig);
            if (isValid)
            {
                Configuration = profileconfig;
            }

        }

        private bool CheckProfileValidation(ProfileConfigModel profileconfig)
        {
            var result = true;
            if (profileconfig.FaceRecognition == true && profileconfig.FaceId == null)
            {
                ErrorService.GetInstance.SendNotificationMessage("User Authentication Image missing.");
                result = false;
            }


            return result;
        }
    }
}
