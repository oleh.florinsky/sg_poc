﻿namespace SessionGuardian.BLL.Services.Secure
{
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.Common.Helper;
    using System.IO;
    using System.Reflection;

    public class CredentialService
    {
        private static string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\UserConfig.xml";
        public static bool HasCredential = File.Exists(location);


        public static void SaveCredential(UserConfiguration protocolCredential)
        {

            // clear it`s content
            File.WriteAllText(location, "");

            XmlHelper.ToXmlFile(protocolCredential, location); // storage credentials in xml file
            var securityServie = new SecurityService();

            securityServie.EncryptXMLElement(location, ContainerName.XML_RSA_KEY_USER_CONFIGURATION.ToString(), nameof(UserConfiguration));
            LogService.Info("Configuration encrypted", true, nameof(CredentialService));
        }

        public static UserConfiguration GetUserConfiguration()
        {
            var securityServie = new SecurityService();
            var xmlData = securityServie.DecryptXMLElement(location, ContainerName.XML_RSA_KEY_USER_CONFIGURATION.ToString());
            var result = XmlHelper.FromXml<UserConfiguration>(xmlData);
            return result;
        }
    }
}
