﻿namespace SessionGuardian.BLL.Services.Secure
{
    using System;
    using System.Security.Cryptography;
    using System.Xml;

    public class SecurityService
    {

        public void EncryptXMLElement(string fileName, string containerName, string encryptElement) // Encrypt XML Elements with Asymmetric Keys
        {
            // Create an XmlDocument object.
            XmlDocument xmlDoc = new XmlDocument();

            // Load an XML file into the XmlDocument object.

            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(fileName);



            // Create a new CspParameters object to specify
            // a key container.
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = containerName;

            // Create a new RSA key and save it in the container.  This key will encrypt
            // a symmetric key, which will then be encryped in the XML document.
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);

            try
            {
                // Encrypt the "creditcard" element.
                SecurityXMLService.Encrypt(xmlDoc, encryptElement, "EncryptedElement", rsaKey, "rsaKey");


                // Save the XML document.
                xmlDoc.Save(fileName);
                Console.WriteLine("Encrypted XML:");
                Console.WriteLine();
                Console.WriteLine(xmlDoc.OuterXml);

                //DecryptXMLElement(fileName, containerName);
                //SecurityXML.Decrypt(xmlDoc, rsaKey, "rsaKey");
                //xmlDoc.Save(fileName);
                //// Display the encrypted XML to the console.
                //Console.WriteLine();
                //Console.WriteLine("Decrypted XML:");
                //Console.WriteLine();
                //Console.WriteLine(xmlDoc.OuterXml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // Clear the RSA key.
                rsaKey.Clear();
            }
        }

        public string DecryptXMLElement(string fileName, string containerName) // Encrypt XML Elements with Asymmetric Keys
        {
            // Create an XmlDocument object.
            XmlDocument xmlDoc = new XmlDocument();

            // Load an XML file into the XmlDocument object.
            try
            {
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = containerName;

            // Get the RSA key from the key container.  This key will decrypt
            // a symmetric key that was imbedded in the XML document.
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);

            try
            {
                // Decrypt the elements.
                SecurityXMLService.Decrypt(xmlDoc, rsaKey, "rsaKey");

                // Save the XML document.
                //xmlDoc.Save("UserConfig.xml");

                // Display the encrypted XML to the console.
                Console.WriteLine("Decrypted XML:");
                Console.WriteLine(xmlDoc.OuterXml);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // Clear the RSA key.
                rsaKey.Clear();
            }

            return xmlDoc.OuterXml;

        }
    }
}
