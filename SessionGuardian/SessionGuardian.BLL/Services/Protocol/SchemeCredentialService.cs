﻿namespace SessionGuardian.BLL.Services.Protocol
{
    using Microsoft.Win32;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Services.Logs;
    using System.IO;
    using System.Reflection;

    public class SchemeCredentialService
    {
        private static string nameScheme => "sg";
        private static string customLink;

        public static ProtocolCredentialModel GetCredentialFromLink(string link) // example: sg://testRestToken
        {
            customLink = link.Replace(nameScheme + "://", "");
            if (customLink[customLink.Length - 1] == '/')
            {
                customLink = customLink.Remove(customLink.Length - 1);
            }
            string[] items = customLink.Split('@');

            var credentials = new ProtocolCredentialModel()
            {
                JwtToken = items[0],
                //SgToken = items[0],
                //AWSRegCode = items[1],
                //AWSUser = items[2],
                SgEndpoint = items[1],
            };
            return credentials;
        }

        public static void RegisterMyProtocol()
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SessionGuardian.App.exe";

            RegistryKey key = Registry.ClassesRoot.OpenSubKey(nameScheme);  //open protocol's subkey
            if (key == null)
            {
                key = Registry.ClassesRoot.CreateSubKey(nameScheme);
                key.SetValue(string.Empty, "URL: esmApp Protocol");
                key.SetValue("URL Protocol", string.Empty);

                key = key.CreateSubKey(@"shell\open\command");
                key.SetValue(string.Empty, path + " " + "%1"); //%1 represents the argument - this tells windows to open this program with an argument / parameter
            }
            key.Close();
        }
    }
}
