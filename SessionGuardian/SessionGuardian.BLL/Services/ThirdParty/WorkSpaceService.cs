﻿
namespace SessionGuardian.BLL.Services.ThirdParty
{
    using SG.ManagedInjector;
    //using ES.ManagedInjector;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Services.Error;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using WorkSpaceInjector;

    public sealed class WorkSpaceService
    {

        private static readonly object Instancelock = new object();
        private static WorkSpaceService instance = null;

        public int startedProcessId;

        private AppConfiguration appConfig;

        public WorkSpaceService()
        {
            appConfig = AppConfiguration.GetInstance;
        }

        public static WorkSpaceService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new WorkSpaceService();
                        }
                    }
                }
                return instance;
            }
        }

        public void Start()
        {
            appConfig = AppConfiguration.GetInstance;
            var awsStartProtocol = $"workspaces://{appConfig.UserName}@{appConfig.RegistrationCode}";
            var proc = Process.Start(awsStartProtocol);

            startedProcessId = proc.Id;
            Thread.Sleep(4000); // wait until run application

            InjectAssamblyToProcess(proc.Id);

        }

        private void InjectAssamblyToProcess(int processId)
        {
            string injectedAssemblyFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)) + $"\\{typeof(Main).Assembly.GetName().Name}.dll";

            var injectedAssembly = Assembly.LoadFile(injectedAssemblyFile);

            var injector = new Injector(processId, injectedAssembly, "WorkSpaceInjector.Main.Run");
            var injectionResult = injector.Inject();
            if (injectionResult != InjectionResult.Success)
            {
                ErrorService.GetInstance.SendNotificationMessage("There was an error initiating the Workspaces client");
            }
            Console.WriteLine(injectionResult.ToString() + "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
        }

        public void ShutDownRequiredApps()
        {
            var processList = Process.GetProcessesByName("workspaces");
            foreach (var process in processList)
            {
                process.Kill();
            }
        }
    }
}
