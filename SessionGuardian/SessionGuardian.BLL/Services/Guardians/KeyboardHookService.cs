﻿using SessionGuardian.BLL.Models;
using SessionGuardian.BLL.Services.Profile;
using SessionGuardian.BLL.Services.WebSockets;
using SessionGuardian.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SessionGuardian.BLL.Services.Guardians
{
    public sealed class KeyboardHookService
    {

        private static readonly object Instancelock = new object();
        private static KeyboardHookService instance = null;

        public static KeyboardHookService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new KeyboardHookService();
                        }
                    }
                }
                return instance;
            }
        }


        int hHook;
        Win32Api.HookProc KeyboardHookDelegate;
        public event KeyEventHandler OnKeyDownEvent;
        public event KeyEventHandler OnKeyUpEvent;
        public event KeyPressEventHandler OnKeyPressEvent;

        private static bool blockKeyboardState = false;

        //disable key list
        public  List<Keys> DisableKeys { get; set; } = new List<Keys>();

        public KeyboardHookService()
        {

            //var commonSettings = AppConfiguration.CommonSettings;
            WebSocketService.GetInstance.ReceivedProfileHandler += UpdateKeyboardConfiguration;
            var profileConfiguration = ProfileService.GetInstance;
            SwitchBlockScreenShare(profileConfiguration.Configuration.ScreenShotBlock);
        }

        public void SetHook()
        {

            KeyboardHookDelegate = new Win32Api.HookProc(KeyboardHookProc);

            Process cProcess = Process.GetCurrentProcess();

            ProcessModule cModule = cProcess.MainModule;

            var mh = Win32Api.GetModuleHandle(cModule.ModuleName);

            hHook = Win32Api.SetWindowsHookEx(Win32Api.WH_KEYBOARD_LL, KeyboardHookDelegate, mh, 0);
        }

        public void UnHook()
        {
            Win32Api.UnhookWindowsHookEx(hHook);
        }

        private List<Keys> preKeysList = new List<Keys>();//store the press keys

        private int KeyboardHookProc(int nCode, Int32 wParam, IntPtr lParam)
        {
            //fitter the invalid message
            if ((nCode >= 0) && (OnKeyDownEvent != null || OnKeyUpEvent != null || OnKeyPressEvent != null))
            {
                KeyboardHookStruct KeyDataFromHook = (KeyboardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyboardHookStruct));
                Keys keyData = (Keys)KeyDataFromHook.vkCode;

                //control key up
                if ((OnKeyDownEvent != null || OnKeyPressEvent != null) && (wParam == Win32Api.WM_KEYDOWN || wParam == Win32Api.WM_SYSKEYDOWN))
                {
                    if (IsCtrlAltShiftKeys(keyData) && preKeysList.IndexOf(keyData) == -1)
                    {
                        preKeysList.Add(keyData);
                    }
                }

                //WM_KEYDOWN and WM_SYSTEMKEYDOWN trigger OnKeyDownEvent
                if (OnKeyDownEvent != null && (wParam == Win32Api.WM_KEYDOWN || wParam == Win32Api.WM_SYSKEYDOWN))
                {
                    if (DisableKeys.Count > 0)
                    {
                        //fitter key
                        if (DisableKeys.Contains((Keys)KeyDataFromHook.vkCode))
                            return 1;
                    }

                    KeyEventArgs e = new KeyEventArgs(GetDownKeys(keyData));

                    OnKeyDownEvent(this, e);
                }
                //WM_KEYDOWN trigger OnKeyPressEvent

                if (OnKeyPressEvent != null && wParam == Win32Api.WM_KEYDOWN)
                {

                    byte[] keyState = new byte[256];
                    Win32Api.GetKeyboardState(keyState);

                    byte[] inBuffer = new byte[2];

                    if (Win32Api.ToAscii(KeyDataFromHook.vkCode, KeyDataFromHook.scanCode, keyState, inBuffer, KeyDataFromHook.flags) == 1)
                    {
                        KeyPressEventArgs e = new KeyPressEventArgs((char)inBuffer[0]);
                        OnKeyPressEvent(this, e);
                    }
                }

                //control key up
                if ((OnKeyDownEvent != null || OnKeyPressEvent != null) && (wParam == Win32Api.WM_KEYUP || wParam == Win32Api.WM_SYSKEYUP))
                {
                    if (IsCtrlAltShiftKeys(keyData))
                    {
                        for (int i = preKeysList.Count - 1; i >= 0; i--)
                        {
                            if (preKeysList[i] == keyData) { preKeysList.RemoveAt(i); }
                        }
                    }
                }

                //WM_KeyUp and WM_SYSKEYUP trigger OnKeyUp Event
                if (OnKeyUpEvent != null && (wParam == Win32Api.WM_KEYUP || wParam == Win32Api.WM_SYSKEYUP))
                {
                    KeyEventArgs e = new KeyEventArgs(GetDownKeys(keyData));
                    OnKeyUpEvent(this, e);
                }
            }
            return Win32Api.CallNextHookEx(hHook, nCode, wParam, lParam);
        }

        //key GetKeyDown's key
        private Keys GetDownKeys(Keys key)
        {
            Keys rtnKey = Keys.None;
            foreach (Keys i in preKeysList)
            {
                if (i == Keys.LControlKey || i == Keys.RControlKey) { rtnKey = rtnKey | Keys.Control; }
                if (i == Keys.LMenu || i == Keys.RMenu) { rtnKey = rtnKey | Keys.Alt; }
                if (i == Keys.LShiftKey || i == Keys.RShiftKey) { rtnKey = rtnKey | Keys.Shift; }
            }
            return rtnKey | key;
        }

        //Is Combo key
        private Boolean IsCtrlAltShiftKeys(Keys key)
        {
            if (key == Keys.LControlKey || key == Keys.RControlKey || key == Keys.LMenu
                || key == Keys.RMenu || key == Keys.LShiftKey || key == Keys.RShiftKey)
            {
                return true;
            }
            return false;
        }

        public  void SwitchBlockScreenShare(bool block)
        {
            if (block)
            {
                var printScreenKey = DisableKeys.Find(u => u.Equals(Keys.PrintScreen));

                if (printScreenKey == Keys.None)
                {
                    DisableKeys.Add(Keys.PrintScreen);
                }
            }
            else
            {
                DisableKeys.Remove(Keys.PrintScreen);
            }
            Console.WriteLine($"?????????????????????????????????????????????????????????????????<<<<<<<<<<<<<<<<<<<<< Press button event >>>>>>>>>>>>>>>>>{block}");

        }

        //public static void SwitchKeyboardBlock(bool block)
        //{
        //    if (blockKeyboardState != block)
        //    {
        //        var keys = Enum.GetValues(typeof(Keys)).Cast<Keys>();
        //        if (block)
        //        {
        //            DisableKeys.AddRange(keys);
        //            Console.WriteLine("lock keyboard---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        //        }
        //        else
        //        {
        //            foreach (var item in keys)
        //            {
        //                DisableKeys.Remove(item);

        //            }
        //            Console.WriteLine("unlock keyboard------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        //        }
        //    }
        //    blockKeyboardState = block;

        //    SwitchBlockScreenShare(ProfileService.Configuration.ScreenShotBlock);
        //}

        private void UpdateKeyboardConfiguration(ProfileConfigModel profileConfig)
        {
            SwitchBlockScreenShare(profileConfig.ScreenShotBlock);
        }

    }
}
