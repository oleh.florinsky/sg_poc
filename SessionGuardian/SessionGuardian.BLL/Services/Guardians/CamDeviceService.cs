﻿namespace SessionGuardian.BLL.Services.Guardians
{
    using AForge.Video;
    using AForge.Video.DirectShow;
    using SessionGuardian.BLL.Models.Defender;
    using SessionGuardian.BLL.Models.Guardians;
    using SessionGuardian.BLL.Services.Profile;
    using System;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Linq;
    using System.Reactive;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;

    public sealed class CamDeviceService
    {

        private bool snapShotFired;
        private static readonly object Instancelock = new object();
        private static CamDeviceService instance = null;
        public VideoCaptureDevice cam;
        private static FilterInfoCollection webcam;
        private static int count = 0;
        private static DateTime stampDateTime = DateTime.Now;
        private Bitmap bit;
        public ISubject<BaseCommand> WebCamStateChange;

        public static CamDeviceService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new CamDeviceService();
                        }
                    }
                }
                return instance;
            }
        }

        public CamDeviceService()
        {
            webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            cam = new VideoCaptureDevice(webcam[0].MonikerString);
        }


        public void StartMonitoring()
        {
            if (HasConnectedDevice())
            {
                cam.NewFrame += Cam_NewFrame;
                cam.PlayingFinished += Cam_PlayingFinished;
                cam.Start();

                snapShotFired = false;
                stampDateTime = DateTime.Now.AddMilliseconds(5000);


                Task.Run(() =>
                {
                    while (!snapShotFired) // waiting until fired the first snapshot
                    {
                        if (DateTime.Now >= stampDateTime)
                        {
                            WebCamStateChange.OnNext(new CamDeviceCommand("Looks like the webcam using by other applciation"));
                            break;
                        }
                        else
                        {
                            Console.WriteLine($"waiting the first snapshot...{DateTime.Now.Second}");
                            Thread.Sleep(100);
                        }
                    }
                });
            }
        }

        private void VideoSourceOnNewFrame(object sender, NewFrameEventArgs eventargs)
        {
            throw new NotImplementedException();
        }

        private void Cam_PlayingFinished(object sender, ReasonToFinishPlaying reason)
        {
            if (ReasonToFinishPlaying.StoppedByUser != reason)
            {
                WebCamStateChange.OnNext(new CamDeviceCommand(reason.ToString()));
            }
        }


        public void StopMonitoring()
        {
            cam.NewFrame -= Cam_NewFrame;
            cam.PlayingFinished -= Cam_PlayingFinished;
            cam.SignalToStop();
            cam.WaitForStop();

        }


        public void ActivateSubscription<T>(Action<BaseCommand> action)
        {
            WebCamStateChange = new Subject<BaseCommand>();
            WebCamStateChange.Subscribe(new AnonymousObserver<BaseCommand>(action, onCompleted: OnCompleted));
        }
        public static void OnCompleted()
        {
            Console.WriteLine("END WORK SUBSCRIPTION OF REACTIVEEXTENSION CaDeviceService");
        }
        [DllImport("gdi32.dll", EntryPoint = "DeleteObject", SetLastError = true)]
        static extern bool DeleteObject(IntPtr hObject);

        private void Cam_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {


            try
            {
                snapShotFired = true;
                if (DateTime.Now > stampDateTime)
                {
                    stampDateTime = DateTime.Now.AddMilliseconds(100);
                    bit = (Bitmap)eventArgs.Frame.Clone();
                    //var handle = bit.GetHbitmap();
                    //DeleteObject(handle);

                    count++;
                    SnapshotModel.GetInstance.UpdateActualSnapshot(bit);
                    SnapshotModel.GetInstance.UpdateActualIntt(count);
                    //bit.Dispose();

                    SnapshotModel.GetInstance.UpdateFlag = true; // should be set after update snapshot!

                    //Console.WriteLine($"took snapshot - {DateTime.Now.ToString()}");
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }





        public static bool HasConnectedDevice()
        {
            var videoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                videoDevices.Add(filterInfo);
            }
            if (videoDevices.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public static class CamExtension
    {
        public static IObservable<T> AsObservable<T>(this VideoCaptureDevice cam, T action)
        {
            return Observable.Create<T>(obs =>
            {
                IDisposable disposable = Observable.FromEvent<NewFrameEventHandler, T>(
                    e => cam.NewFrame += e,
                    e => cam.NewFrame -= e
                    )
                    .Finally(cam.Stop)
                    .Subscribe(obs);
                cam.Start();
                return disposable;
            });
        }
    }
}

