﻿using SessionGuardian.BLL.Models.Defender;
using SessionGuardian.BLL.Services.Profile;
using SessionGuardian.BLL.Services.ThirdParty;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Reactive;
using System.Reactive.Subjects;

namespace SessionGuardian.BLL.Services.Guardians
{
    public sealed class ProcessService
    {

        public ISubject<BaseCommand> ProcessChange;
        private static readonly object Instancelock = new object();
        private static ProcessService instance = null;

        ManagementEventWatcher processStartEvent;

        public static ProcessService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new ProcessService();
                        }
                    }
                }
                return instance;
            }
        }

        public ProcessService()
        {
            processStartEvent = new ManagementEventWatcher("SELECT * FROM Win32_ProcessStartTrace");
        }

        public void ActivateSubscription<T>(Action<BaseCommand> action)
        {
            ProcessChange = new Subject<BaseCommand>();
            ProcessChange.Subscribe(new AnonymousObserver<BaseCommand>(action, onCompleted: OnCompleted));
        }

        public static void OnCompleted()
        {
            Console.WriteLine("END WORK SUBSCRIPTION OF REACTIVEEXTENSION");
        }

        public void StartMonitoring()
        {
            processStartEvent.EventArrived += new EventArrivedEventHandler(processStartEvent_EventArrived);
            processStartEvent.Start();
        }


        public void StopMonitoring()
        {
            processStartEvent.EventArrived -= new EventArrivedEventHandler(processStartEvent_EventArrived);
            processStartEvent?.Stop();
        }

        void processStartEvent_EventArrived(object sender, EventArrivedEventArgs e)
        {

            string processName = e.NewEvent.Properties["ProcessName"].Value.ToString();
            string processID = Convert.ToInt32(e.NewEvent.Properties["ProcessID"].Value).ToString();
            Console.WriteLine("Process started. Name: " + processName + " | ID: " + processID);

            var startingForbiddenProcesses = ProfileService.GetInstance.Configuration.ForbiddenApps.Where(x => (x + ".exe") == processName);


            if (startingForbiddenProcesses.Count() > 0)
            {
                processStartEvent.Stop();
                ProcessChange.OnNext(new ForbidProcessCommand($"Blacklisted Application Detected: {processName}"));
            }

            if (processName == "workspaces.exe") // forbid the new instance for Workspace application
            {

                KillProcessByName("workspaces");
            }
        }
        private void KillProcessByName(string name)
        {
            var processList = Process.GetProcessesByName(name);
            foreach (var proccess in processList)
            {
                var ignoreWorkSpaceId = WorkSpaceService.GetInstance.startedProcessId;
                if (ignoreWorkSpaceId != proccess.Id)
                {
                    proccess.Kill();
                    Console.WriteLine($"Process handle kill. Name: {name}");
                }
            }
        }

        public bool CheckStartedForbiddenProcesses(string[] forbiddenList)
        {
            var allProcesses = Process.GetProcesses().Select(x => x.ProcessName).ToList();
            return allProcesses.Intersect(forbiddenList).ToList().Count > 0 ? false : true;
        }
    }
}
