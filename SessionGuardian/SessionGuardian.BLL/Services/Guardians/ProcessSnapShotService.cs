﻿using SessionGuardian.BLL.Enums;
using SessionGuardian.BLL.Interfaces.Luxand;
using SessionGuardian.BLL.Models.Defender;
using SessionGuardian.BLL.Models.Guardians;
using SessionGuardian.BLL.Services.AWS;
using SessionGuardian.BLL.Services.LuxandRecognition;
using SessionGuardian.BLL.Services.Profile;
using System;
using System.Reactive;
using System.Reactive.Subjects;
using System.Threading;

namespace SessionGuardian.BLL.Services.Guardians
{


    public sealed class ProcessSnapShotService
    {
        ILuxandRecognitionInterface luxandService;

        public ISubject<BaseCommand> RecognitionStateChange;

        private int gazeDetectedCount = 0;



        public AWSService _awsService;
        private static readonly object Instancelock = new object();
        private static ProcessSnapShotService instance = null;
        private static RecognizerStatus lastRecognizeStatus = RecognizerStatus.DEFAULT;

        public static ProcessSnapShotService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new ProcessSnapShotService();
                        }
                    }
                }
                return instance;
            }
        }

        public ProcessSnapShotService()
        {
            luxandService = LuxandRecognitionService.GetInstance;
            _awsService = new AWSService();
        }

        public void StartHandleSnapshot(CancellationToken token)
        {
            try
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        lastRecognizeStatus = RecognizerStatus.DEFAULT;
                        break;
                    }
                    if (SnapshotModel.GetInstance.UpdateFlag)
                    {
                        SnapshotModel.GetInstance.UpdateFlag = false;

                        var start = DateTime.Now;
                        
                        var handle = SnapshotModel.GetInstance.GetActualSnapshot();
                        using (handle)
                        {
                            var safeHandle = handle.DangerousGetHandle();
                            HandleActualSnapShot(safeHandle);
                        }
                        int different = (int)(DateTime.Now - start).TotalMilliseconds;
                        int sleepTimeMs = 300;
                        if (sleepTimeMs > different)
                        {
                            Thread.Sleep(300 - different);
                        }
                     
                        Console.WriteLine(different);
                    }
                    else
                    {
                        Thread.Sleep(50);
                    }
                }
            }
            catch (Exception ex)
            {


            }
        }


        public void ActivateSubscription<T>(Action<BaseCommand> action)
        {
            RecognitionStateChange = new Subject<BaseCommand>();
            RecognitionStateChange.Subscribe(new AnonymousObserver<BaseCommand>(action, onCompleted: OnCompleted));
        }

        public static void OnCompleted()
        {
            Console.WriteLine("END WORK SUBSCRIPTION OF REACTIVEEXTENSION");
        }


        private void HandleActualSnapShot(IntPtr handle)
        {
            //var bitmapHandle = snapshot.GetHbitmap();
            try
            {
                //MemoryStream ms = new MemoryStream();
                //snapshot.Save(ms, ImageFormat.Jpeg);


                //var img = new Amazon.Rekognition.Model.Image();
                //img.Bytes = new MemoryStream(ms.ToArray());

                //var start = DateTime.Now;


                var detectResult = LuxandRecognitionService.GetInstance.DetectFaceAsync(handle);



                //int different = (int)(DateTime.Now - start).TotalMilliseconds;
                //Console.WriteLine(different);

                //var detectFaceTask = _awsService.DetectFaceAsync(img);
                //var detectForbidObjectTask = _awsService.DetectLabelsAsync(img);

                //await Task.WhenAll(detectFaceTask, detectForbidObjectTask);
                //var facesOutput = detectFaceTask.Result;

                if (detectResult.Count > 0)
                {
                    var profileConfiguration = ProfileService.GetInstance.Configuration;

                    //var devicesOutput = detectForbidObjectTask.Result;
                    var devicesOutput = 0;
                    if (devicesOutput > 0) // skip when was find devices
                    {
                        //string message = "";
                        //if (profileConfiguration.DisplayDetectedObjects)
                        //{
                        //    StringBuilder sb = new StringBuilder();
                        //    for (int i = 0; i < devicesOutput.Count; i++)
                        //    {
                        //        sb.Append($"{devicesOutput[i]}, ");
                        //    }
                        //    var deviceList = sb.ToString();
                        //    message = $"Detected a forbidden device: {deviceList.Remove(deviceList.Length - 2, 2)}";
                        //}
                        //UpdateGuardianState(message, RecognizerStatus.DETECTED_FORBIDDEN_DEVICE, false);
                    }
                    else
                    {
                        var compareResult = LuxandRecognitionService.GetInstance.FindAnchorFace();
                        var isOpenEyes = profileConfiguration.GazeDetection ? compareResult.OpenEyes : false;
                        if (isOpenEyes)
                        {
                            if (profileConfiguration.FaceRecognition)
                            {
                                //var responce = await _awsService.CompareFacesAsync(img);
                                //Console.WriteLine($"({count})1.3________________________________ {gazeDetectedCount}_________________________COMPARE_FACES__{DateTime.Now.ToString()}");
                                if (compareResult.HasTargetFace)
                                {
                                    if (profileConfiguration.ContentRestriction && detectResult.Count > 1) // if true show content for several people
                                    {
                                        UpdateGuardianState("Detected unauthorized user(s)", RecognizerStatus.DETECTED_MORE_FACES);
                                    }
                                    else
                                    {
                                        UpdateGuardianState("", RecognizerStatus.DETECTED_COINCIDENCE_FACE); //HIDE 
                                    }
                                    //else
                                    //{
                                    //    if (devicesOutput > 1)
                                    //    {
                                    //        UpdateGuardianState("Detected unauthorized user(s)", RecognizerStatus.DETECTED_MORE_FACES);
                                    //    }
                                    //    else
                                    //    {
                                    //        UpdateGuardianState("", RecognizerStatus.DETECTED_COINCIDENCE_FACE); //HIDE 
                                    //    }
                                    //    //if (detectResult.Count > 1 && responce.FaceMatches[0].Face.FaceId == profileConfiguration.FaceId) //Check that snapshot contains only one authorized user and compare the FaceId of user
                                    //    //{
                                    //    //    UpdateGuardianState("Detected unauthorized user(s)", RecognizerStatus.DETECTED_MORE_FACES);
                                    //    //}
                                    //    //else
                                    //    //{
                                    //    //    UpdateGuardianState("", RecognizerStatus.DETECTED_COINCIDENCE_FACE); //HIDE 
                                    //    //}
                                    //}
                                }
                                else
                                {
                                    UpdateGuardianState("Authorized user not found", RecognizerStatus.NOT_DETECT_FACE);
                                }
                            }
                            else
                            {
                                UpdateGuardianState("", RecognizerStatus.DETECTED_COINCIDENCE_FACE); //HIDE 
                            }
                        }
                        else
                        {
                            UpdateGuardianState("Detected the closed eyes", RecognizerStatus.NOT_OPEN_EYES);
                        }
                    }
                }
                else
                {
                    UpdateGuardianState("Faces not found", RecognizerStatus.NOT_DETECT_FACE);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            //var start = DateTime.Now;
            //int different = (int)(DateTime.Now - start).TotalMilliseconds;



        }

        private void UpdateGuardianState(string message, RecognizerStatus status, bool ignoreRepetition = true)
        {
            if (!(status == lastRecognizeStatus) || !ignoreRepetition)
            {
                lastRecognizeStatus = status;
                RecognitionStateChange.OnNext(new RecognitionCommand(status, ignoreRepetition, message));
            }
        }
    }
}
