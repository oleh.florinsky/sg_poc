﻿namespace SessionGuardian.BLL.Services.WebSockets
{
    using Newtonsoft.Json;
    using ObjectsComparer;
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models;
    using SessionGuardian.BLL.Models.AWS;
    using SessionGuardian.BLL.Models.Sockets;
    using SessionGuardian.BLL.Models.Sockets.Responce;
    using SessionGuardian.BLL.Services.AWS;
    using SessionGuardian.BLL.Services.Error;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.Profile;
    using SessionGuardian.BLL.Services.Secure;
    using SessionGuardian.Common;
    using SessionGuardian.Common.Enums.WebSockets;
    using SessionGuardian.Common.Models;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.WebSockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;


    public sealed class WebSocketService
    {

        public delegate void ReceivedAwsToken();
        public event ReceivedAwsToken ReceivedAwsTokenHandler;

        public delegate void ReceivedServerMessage();
        public event ReceivedServerMessage ReceivedServerMessageHandler;

        public delegate void ReceivedProfile(ProfileConfigModel data);
        public event ReceivedProfile ReceivedProfileHandler;


        private static readonly object Instancelock = new object();
        private static WebSocketService instance = null;

        public static WebSocketService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new WebSocketService();
                        }
                    }
                }
                return instance;
            }
        }

        private static ConcurrentQueue<string> _received = new ConcurrentQueue<string>();
        private static ConcurrentQueue<Tuple<SocketRequestType, RequestModel>> _sendqueue = new ConcurrentQueue<Tuple<SocketRequestType, RequestModel>>();

        private static Task _receiveTask;
        private static Task _sendTask;

        ObjectsComparer.Comparer<ProfileConfigModel> _comparer;

        private volatile SocketMessageType waitResponse;

        public ClientWebSocket Client { get; set; }

        public WebSocketService()
        {
            Client = new ClientWebSocket();
            _receiveTask = Receive();
            _sendTask = SendQueueObserver();


            _comparer = new ObjectsComparer.Comparer<ProfileConfigModel>(
                        new ComparisonSettings
                        {
                            //Null and empty error lists are equal  
                            //EmptyAndNullEnumerablesEqual = true
                        });
        }



        public async Task Connect(string endPoint, string token)
        {
            try
            {
                // allow HTTPS connection
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                await Client.ConnectAsync(new Uri("wss://" + endPoint + $"?token={token}"), CancellationToken.None);
                Console.WriteLine("????????????? Set new connection by socket ????????????????");
                var userConfiguration = CredentialService.GetUserConfiguration();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async Task Send(RequestModel request)
        {
            try
            {
                if (Client.State == WebSocketState.Open)
                {
                    var sendBuffer = new ArraySegment<byte>(request.Message);

                    var sender = Client.SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    Console.WriteLine($"_____________________________________________________SEND REQUEST SOCKET__________________________________________{request.MessageTypeAlias}__________{DateTime.Now}");
                    await sender;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private async Task Receive()
        {
            try
            {
                var buffer = new byte[1024 * 32];
                while (true)
                {
                    if (Client != null)
                    {
                        if (Client.State == WebSocketState.Open || Client.State == WebSocketState.CloseSent)
                        {
                            var receiveBuffer = new ArraySegment<byte>(buffer);
                            var result = await Client.ReceiveAsync(receiveBuffer, CancellationToken.None);
                            switch (result.MessageType)
                            {
                                case WebSocketMessageType.Binary:
                                    break;
                                case WebSocketMessageType.Close:
                                    Console.WriteLine("_____________________________________________________BAD RESPONCE SOCKET____________________________________________________");
                                    Console.WriteLine(result.CloseStatusDescription);

                                    if (result.CloseStatus != WebSocketCloseStatus.NormalClosure)
                                    {
                                        HandleBadResponse(result);
                                    }
                                    else
                                    {
                                        Client = new ClientWebSocket();
                                    }
                                    LogService.Error($"Socket close reason: {result.CloseStatusDescription}", true, nameof(WebSocketService));
                                    break;
                                case WebSocketMessageType.Text:
                                    Console.WriteLine($"_____________________________________________________RECEIVE DATA BY SOCKET____________________________________________________{DateTime.Now}");
                                    ReceivedServerMessageHandler.Invoke();
                                    string content = Encoding.UTF8.GetString(buffer, 0, result.Count);
                                    Console.WriteLine(content);

                                    //MyEnum myEnum = (MyEnum)Enum.Parse(typeof(MyEnum), myString);


                                    var serverMessage = DeserializeObject<ResponseMessageModel>(content);

                                    SocketMessageType myEnum = (SocketMessageType)Enum.Parse(typeof(SocketMessageType), serverMessage.Type);

                                    if (myEnum == waitResponse)
                                    {
                                        waitResponse = SocketMessageType.NO_RESPONSE;
                                        _received.Enqueue(content);
                                    }
                                    else
                                    {
                                        HandleReceivedMessage(content);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            await Task.Delay(100);
                        }
                    }
                }
            }
            catch (WebSocketException ex) // Reset socket configuration after lose Internet connection
            {
                LogService.Error($"Error: {ex.Message}", className: nameof(WebSocketService));
                if (Client.State == WebSocketState.Aborted)
                {
                    Client.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None).GetAwaiter().GetResult();
                    Client = new ClientWebSocket();
                    _receiveTask = Receive(); // Activate receiver
                }
                ErrorService.GetInstance.StopSecureSession();

            }
        }

        private void HandleReceivedMessage(string message)
        {
            var serverMessage = DeserializeObject<ResponseMessageModel>(message);
            SocketMessageType messageType = (SocketMessageType)Enum.Parse(typeof(SocketMessageType), serverMessage.Type);
            switch (messageType)
            {
                case SocketMessageType.UPDATE_PROFILE_CONFIG_EVENT:
                    var receivedProfileConfig = DeserializeObject<ProfileConfigModel>(serverMessage.Payload.ToString());
                    var profileConfiguration = ProfileService.GetInstance;
                    if (profileConfiguration.Configuration != null)
                    {
                        IEnumerable<Difference> differences;
                        var profileHasChanges = _comparer.Compare(receivedProfileConfig, profileConfiguration.Configuration, out differences);

                        var count = differences.ToList().Count;
                        foreach (var item in differences.ToList())
                        {
                            Console.WriteLine(item.DifferenceType);
                        }

                        if (!profileHasChanges) // if TRUE, update configuration
                        {
                            profileConfiguration.SetProfileConfiguration(receivedProfileConfig);
                            ReceivedProfileHandler.Invoke(profileConfiguration.Configuration);
                        }
                    }
                    break;
                case SocketMessageType.GET_SECURITY_TOKENS_SUCCESS: // update expired token
                    var token = DeserializeObject<AWSCredentialsModel>(serverMessage.Payload.ToString());
                    var awsCredentialService = AWSCredentialService.GetInstance;
                    awsCredentialService.SetCredentials(token);
                    awsCredentialService.StartExpiredTimer();

                    ReceivedAwsTokenHandler?.Invoke();
                    break;


                case SocketMessageType.MESSAGE_PARSE_ERROR:
                    break;
                case SocketMessageType.AUTHENTICATION_SUCCESS:
                    break;
                case SocketMessageType.UPDATE_PROFILE_CONFIG_IMAGE_EVENT:
                    var receivedProfile = DeserializeObject<ProfileConfigModel>(serverMessage.Payload.ToString());
                    var profile = ProfileService.GetInstance;
                    if (profile.Configuration != null)
                    {
                        profile.Configuration.FaceId = receivedProfile.FaceId;
                    }
                    Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    break;
            }
        }


        private void HandleBadResponse(WebSocketReceiveResult badResult)
        {
            string badMessage = "";
            switch ((int)badResult.CloseStatus)
            {
                case (int)BadResponseStatus.FORBIDDEN:
                    badMessage = BadResponseMessageModel.Forbidden;
                    break;
                case (int)BadResponseStatus.NOT_FOUND:
                    badMessage = BadResponseMessageModel.NotFound;
                    break;
                case (int)BadResponseStatus.SESSION_RESTARTED: // react - closing application because new instance was opened on another device using the same credentials
                    AppService.ShutdownApp();
                    break;
                case (int)BadResponseStatus.NOT_ALLOWED_IP:
                    badMessage = BadResponseMessageModel.NotAllowedIP;
                    break;
                case (int)BadResponseStatus.CONFIGURATION_NOT_FOUND:
                    badMessage = BadResponseMessageModel.ConfigurationNotFound;
                    break;
                case (int)BadResponseStatus.CONFIGURATION_NOT_ACTIVE:
                    badMessage = BadResponseMessageModel.ConfigurationNotActive;
                    break;
                default:
                    badMessage = BadResponseMessageModel.UnknownStatus;
                    break;
            }
            ErrorService.GetInstance.SendNotificationMessage(badMessage);
        }



        private async Task SendQueueObserver()
        {
            try
            {
                while (true)
                {
                    Tuple<SocketRequestType, RequestModel> request;
                    if (_sendqueue.TryPeek(out request))
                    {
                        if (Client.State == WebSocketState.Open)
                        {
                            switch (request.Item1)
                            {
                                case SocketRequestType.SendMessage:
                                    await Send(request.Item2).ConfigureAwait(false);
                                    break;

                                case SocketRequestType.CloseOutputConnection:
                                    await Client.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                                    break;
                            }
                        }
                        _sendqueue.TryDequeue(out request);
                    }
                    else
                    {
                        await Task.Delay(100);
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.Error($"Error: {ex.Message}", className: nameof(WebSocketService));
                ErrorService.GetInstance.StopSecureSession();
                //if (Client.State == WebSocketState.Aborted)
                //{
                //    Client.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None).GetAwaiter().GetResult();
                //    Client = new ClientWebSocket();
                //    _sendTask = SendQueueObserver(); // Activate sender
                //}
                //ErrorService.GetInstance.StopSecureSession();
                //throw;
            }
        }


        private static T DeserializeObject<T>(string body)
           where T : class
        {
            return JsonConvert.DeserializeObject<T>(body);
        }



        public bool TryGetReceivedMessage<T>(out T typedMessage)
        {
            typedMessage = default(T);
            string message = null;

            if (_received.Count == 0)
                return false;

            var result = _received.TryDequeue(out message);
            try
            {
                typedMessage = JsonConvert.DeserializeObject<T>(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public RequestModel CreateRequestMessage<T>(T message, string type)
        {
            string json = JsonConvert.SerializeObject(message);

            return new RequestModel()
            {
                Message = Encoding.UTF8.GetBytes(json),
                MessageTypeAlias = type
            };
        }



        public void SendMessage(SocketRequestType requestType, RequestModel requestModel, SocketMessageType waitResponse = SocketMessageType.NO_RESPONSE)
        {
            this.waitResponse = waitResponse;
            var request = new Tuple<SocketRequestType, RequestModel>(requestType, requestModel);
            _sendqueue.Enqueue(request);
        }



        public async Task<T> SendMessage<T>(SocketMessageType type, string alias, CancellationToken token, SocketMessageType expectedResonseType = SocketMessageType.NO_RESPONSE, string payload = "", SocketRequestType requestType = SocketRequestType.SendMessage)
        {
            this.waitResponse = expectedResonseType;
            T result = default;
            if (Client.State == WebSocketState.Open)
            {
                var message = new RequestMessageModel()
                {
                    Type = type.ToString(),
                    Payload = payload
                };
                var requestMessage = CreateRequestMessage(message, alias);
                var request = new Tuple<SocketRequestType, RequestModel>(requestType, requestMessage);
                _sendqueue.Enqueue(request);


                if (expectedResonseType != SocketMessageType.NO_RESPONSE)
                {
                    ResponseMessageModel response;
                    while (!TryGetReceivedMessage(out response))
                    {
                        if (token.IsCancellationRequested)
                        {
                            Console.WriteLine($"############################################## TIME OUT IS OVER ###{alias}##########################################################{nameof(WebSocketService)}");
                            return result;
                        }
                        else
                        {
                            await Task.Delay(100);
                        }
                    }
                    result = JsonConvert.DeserializeObject<T>(response.Payload.ToString());
                }
            }
            return result;
        }

        public void SendLogMessage(string message)
        {
            var logMessage = new RequestMessageModel()
            {
                Type = SocketMessageType.LOG.ToString(),
                Payload = message
            };

            var logRequest = CreateRequestMessage(logMessage, "log");
            var request = new Tuple<SocketRequestType, RequestModel>(SocketRequestType.SendMessage, logRequest);
            _sendqueue.Enqueue(request);
        }


        public void CloseConnection()
        {
            var closeOutputQueueItem = new Tuple<SocketRequestType, RequestModel>(SocketRequestType.CloseOutputConnection, null);
            _sendqueue.Enqueue(closeOutputQueueItem);
        }
    }
}
