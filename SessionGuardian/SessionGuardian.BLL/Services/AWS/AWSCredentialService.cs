﻿namespace SessionGuardian.BLL.Services.AWS
{
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models.AWS;
    using SessionGuardian.BLL.Models.Sockets;
    using SessionGuardian.BLL.Services.WebSockets;
    using SessionGuardian.Common.Enums.WebSockets;
    using System;
    using System.Timers;

    public sealed class AWSCredentialService
    {
        public AWSCredentialsModel Credentials { get; set; }
        private WebSocketService webSocketService;


        private Timer timer;

        private static readonly object Instancelock = new object();
        private static AWSCredentialService instance = null;

        public static AWSCredentialService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new AWSCredentialService();
                        }
                    }
                }
                return instance;
            }
        }

        public AWSCredentialService()
        {
            webSocketService = WebSocketService.GetInstance;

            timer = new Timer()
            {
                AutoReset = false
            };
            timer.Elapsed += OnTimedEvent;
        }

        public void SetCredentials(AWSCredentialsModel credentials)
        {
            Credentials = credentials;
        }


        public void StartExpiredTimer()
        {
            var expiryTimeStapm = JavaTimeStampToDateTime(Credentials.Expiration);
            var f = DateTime.Now;
            int expiredMill = (int)(expiryTimeStapm - DateTime.Now).TotalMilliseconds;

            timer.Interval = expiredMill - 15000; //reset tokens before expired token
            timer.Enabled = true;

        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            timer.Stop();
            Console.WriteLine($"start send reset token message {DateTime.Now}");

            var awsTokenMessage = new RequestMessageModel()
            {
                Type = SocketMessageType.GET_SECURITY_TOKENS.ToString(),
                Payload = null
            };

            var awsTokenRequest = webSocketService.CreateRequestMessage(awsTokenMessage, "awsToken");
            webSocketService.SendMessage(SocketRequestType.SendMessage, awsTokenRequest);
        }


        private static DateTimeOffset UnixTimeStampToDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTimeOffset dtDateTime = new DateTimeOffset();
            dtDateTime = DateTimeOffset.FromUnixTimeMilliseconds(unixTimeStamp);
            return dtDateTime;
        }


        public static DateTime JavaTimeStampToDateTime(double javaTimeStamp)
        {
            // Java timestamp is milliseconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(javaTimeStamp).ToLocalTime();
            return dtDateTime;
        }

    }
}
