﻿namespace SessionGuardian.BLL.Services.AWS
{
    using Amazon.Rekognition;
    using Amazon.Rekognition.Model;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.Profile;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AWSService
    {
        public AmazonRekognitionClient rekognitionClient;
        List<string> forbiddenList = new List<string>();

        public AWSService()
        {
            SetClientCredentials();
            //rekognitionClient = new AmazonRekognitionClient(AppConfiguration.GetInstance.AWSCredentials, Amazon.RegionEndpoint.USEast1);
        }


        public void UpdateCredentials()
        {
           
            SetClientCredentials();
        }


        private void SetClientCredentials()
        {
            var awsCredentials = AWSCredentialService.GetInstance.Credentials;

            Console.WriteLine($"Got new aws token************************{awsCredentials.AccessKeyId}*****************************************{awsCredentials.AccessKeyId}*******************************************");
            rekognitionClient = new AmazonRekognitionClient(awsCredentials.AccessKeyId, awsCredentials.SecretAccessKey, awsCredentials.SessionToken, Amazon.RegionEndpoint.GetBySystemName(awsCredentials.Region));
        }


        public async Task<List<string>> DetectLabelsAsync(Image img)
        {
            DetectLabelsResponse detectLabelsResponse;
            try
            {
                DetectLabelsRequest detectlabelsRequest = new DetectLabelsRequest()
                {
                    Image = img,
                    MaxLabels = 200,
                    MinConfidence = 55F
                };
                forbiddenList = ProfileService.GetInstance.Configuration.ForbiddenObjects.ToList();
                detectLabelsResponse = await rekognitionClient.DetectLabelsAsync(detectlabelsRequest);    
            }
            catch (Exception ex)
            {
                LogService.Error(ex.Message, true, nameof(AWSService));
                throw;
            }

            var list = detectLabelsResponse.Labels.Where(x => forbiddenList.Contains(x.Name)).ToList();
            return list.Select(x => x.Name + " (" + x.Confidence + "%)").ToList();
        }

        public async Task<DetectFacesResponse> DetectFaceAsync(Image img)
        {
            try
            {
                DetectFacesRequest dfr = new DetectFacesRequest
                {
                    Image = img,
                    Attributes = new List<string>(new string[] { "ALL" })
                };
                return await rekognitionClient.DetectFacesAsync(dfr);
            }
            catch (Exception ex)
            {
                LogService.Error(ex.Message, true, nameof(AWSService));
                throw;
            }
        }

        public async Task<SearchFacesByImageResponse> CompareFacesAsync(Image img)
        {
            try
            {
                var request = new SearchFacesByImageRequest()
                {
                    CollectionId = ProfileService.GetInstance.Configuration.CollectionId,
                    FaceMatchThreshold = ProfileService.GetInstance.Configuration.FaceMatchConfidence,
                    Image = img,
                    MaxFaces = 1
                };
                return await rekognitionClient.SearchFacesByImageAsync(request);
            }
            catch (Exception ex)
            {
                LogService.Error(ex.Message, true, nameof(AWSService));
                throw;
            }
        }
    }
}
