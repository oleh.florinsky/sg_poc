﻿namespace SessionGuardian.BLL.Models
{
    public class ProfileConfigModel
    {
        public int KeepAliveTimeout { get; set; }

        public string[] ForbiddenObjects { get; set; }

        //public string TargetPhoto { get; set; }

        public string[] ForbiddenApps { get; set; }

        public bool AvCheck { get; set; }

        //public string Bucket { get; set; }

        public bool RequireWebcam { get; set; }

        public int ProcessingSnapshotFrequency { get; set; }

        public bool ScreenShotBlock { get; set; }

        public bool OsCheck { get; set; }

        public bool FaceRecognition { get; set; }

        public int FaceMatchConfidence { get; set; }

        public int ConsecutiveCycles { get; set; }

        public string[] AllowedIPs { get; set; }

        public bool GazeDetection { get; set; }

        public bool DisplayDetectedObjects { get; set; }

        public bool ContentRestriction { get; set; }

        public string CollectionId { get; set; }

        public string FaceId { get; set; }

        public bool AllowVM { get; set; }

        public string ProjectTitle { get; set; }

        public bool UseWatermark { get; set; }
    }
}
