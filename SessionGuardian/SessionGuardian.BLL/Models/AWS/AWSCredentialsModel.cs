﻿namespace SessionGuardian.BLL.Models.AWS
{
    public class AWSCredentialsModel
    {
        public string AccessKeyId { get; set; }

        public long Expiration { get; set; }

        public string SecretAccessKey { get; set; }

        public string SessionToken { get; set; }

        public string Region { get; set; }
    }
}
