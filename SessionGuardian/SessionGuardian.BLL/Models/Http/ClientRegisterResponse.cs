﻿namespace SessionGuardian.BLL.Models.Http
{
    public class ClientRegisterResponse
    {
        public string SgToken { get; set; }
        public string WsConnectionUri { get; set; }
    }
}
