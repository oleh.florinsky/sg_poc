﻿namespace SessionGuardian.BLL.Models.Http
{
    using Newtonsoft.Json;
    using System;

    [Serializable]
    public class ClientRegisterRequest
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
