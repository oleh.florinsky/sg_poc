﻿namespace SessionGuardian.BLL.Models.Workspace
{
    public class WorkspaceModel
    {
        public string RegistrationCode { get; set; }
        public string UserName { get; set; }
    }
}
