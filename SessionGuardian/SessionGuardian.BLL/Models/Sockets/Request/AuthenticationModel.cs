﻿namespace SessionGuardian.BLL.Models.Sockets.Request
{
    using Newtonsoft.Json;
    using System;

    [Serializable]
    class AuthenticationModel
    {
        [JsonProperty("sgToken")]
        public string SgToken { get; set; }

        public AuthenticationModel(string code)
        {
            SgToken = code;
        }
    }
}
