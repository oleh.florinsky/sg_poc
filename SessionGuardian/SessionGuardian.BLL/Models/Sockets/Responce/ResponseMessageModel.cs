﻿namespace SessionGuardian.BLL.Models.Sockets.Responce
{
    public class ResponseMessageModel
    {
        public string Type { get; set; }

        public object Payload { get; set; }

    }
}
