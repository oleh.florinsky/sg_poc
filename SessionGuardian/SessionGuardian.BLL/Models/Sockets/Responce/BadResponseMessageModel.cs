﻿
namespace SessionGuardian.BLL.Models.Sockets.Responce
{
    public class BadResponseMessageModel
    {
        public const string Forbidden = "Authorization denied.";

        public const string NotFound = "Server can`t find target resource";

        public const string SessionRestarted = "There was a connection from another device.";

        //public const string DeactivatedConfiguration = "Current session is stopped because of deactivated configuration";

        public const string NotAllowedIP = "Invalid IP Address detected.";

        public const string ConfigurationNotFound = "Server hasn't found user's configuration.";

        public const string ConfigurationNotActive = "User`s configuration is not active.";

        public const string DefaultNotification = " An application error has occurred";

        public const string UnknownStatus = "Connection to the server is not established.";
    }
}
