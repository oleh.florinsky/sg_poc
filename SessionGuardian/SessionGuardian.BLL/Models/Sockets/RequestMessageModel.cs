﻿namespace SessionGuardian.BLL.Models.Sockets
{
    using Newtonsoft.Json;
    using System;

    [Serializable]
    public class RequestMessageModel
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("payload")]
        public object Payload { get; set; }
    }
}
