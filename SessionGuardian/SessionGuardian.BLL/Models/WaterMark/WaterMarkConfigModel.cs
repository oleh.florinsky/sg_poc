﻿
namespace SessionGuardian.BLL.Models.WaterMark
{
    public class WaterMarkConfigModel
    {
        public string Color { get; set; }

        public string FontFamily { get; set; }

        public int FontSize { get; set; }

        public string fontWeight { get; set; }

        public double Opacity { get; set; }

    }
}
