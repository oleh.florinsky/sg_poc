﻿namespace SessionGuardian.BLL.Models.Recognition
{
    public class CompareFacesOutputModel
    {
        public bool HasTargetFace { get; set; }
        public bool OpenEyes { get; set; }
    }
}
