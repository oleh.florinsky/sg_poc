﻿namespace SessionGuardian.BLL.Models.Recognition
{
    using System.Collections.Generic;

    public class LabelDetectOutputModel
    {
        public List<LabelModel> Faces { get; set; }
        public int Count { get; set; }
    }
}
