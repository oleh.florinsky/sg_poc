﻿namespace SessionGuardian.BLL.Models.Credential
{
    public class ProtocolCredentialModel
    {
        public string JwtToken { get; set; }

        //public string SgToken { get; set; }
        //public string AWSRegCode { get; set; }
        //public string AWSUser { get; set; }
        public string SgEndpoint { get; set; }
    }
}
