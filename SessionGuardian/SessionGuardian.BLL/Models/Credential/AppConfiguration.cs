﻿using SessionGuardian.BLL.Models.Http;

namespace SessionGuardian.BLL.Models.Credential
{
    public sealed class AppConfiguration
    {
        public string RegistrationCode { get; set; }
        public string UserName { get; set; }
        public string SgToken { get; set; }
        public string WsConnectionUri { get; set; }

        private static readonly object Instancelock = new object();
        private static AppConfiguration instance = null;

        public static AppConfiguration GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new AppConfiguration();
                        }
                    }
                }
                return instance;
            }
        }

        public void SetConfiguration(ClientRegisterResponse model)
        {
            SgToken = model.SgToken;
            WsConnectionUri = model.WsConnectionUri;
        }
    }
}
