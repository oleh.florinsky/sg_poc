﻿namespace SessionGuardian.BLL.Models.Credential
{
    public class UserConfiguration
    {
        //public string SgToken { get; set; }
        //public string AWSRegCode { get; set; }
        //public string AWSUser { get; set; }
        public string SgEndpoint { get; set; }
        public string JwtToken { get; set; }
    }
}
