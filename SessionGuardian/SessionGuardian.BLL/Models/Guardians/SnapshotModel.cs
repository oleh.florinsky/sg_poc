﻿using SessionGuardian.BLL.Helpers.UnmanagedCode;
using System;
using System.Drawing;

namespace SessionGuardian.BLL.Models.Guardians
{

    class SnapshotModel
    {

        public bool UpdateFlag { get; set; }
        public Bitmap ActualSnapShot { get; set; }
        public int Value { get; set; }

        private static readonly object Instancelock = new object();
        private static SnapshotModel instance = null;
        public static SnapshotModel GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new SnapshotModel();
                        }
                    }
                }
                return instance;
            }
        }
        public void UpdateActualIntt(int value)
        {
            Value = value;
        }
        public void UpdateActualSnapshot(Bitmap snapShot)
        {
            ActualSnapShot = snapShot;
        }

        public SafeHBitmapHandle GetActualSnapshot()
        {
            IntPtr hbitmap = ActualSnapShot.GetHbitmap();
            return new SafeHBitmapHandle(hbitmap, true);
        }
    }
}
