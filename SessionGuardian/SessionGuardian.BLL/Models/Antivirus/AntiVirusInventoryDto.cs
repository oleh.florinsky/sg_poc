﻿namespace SessionGuardian.BLL.Models.Antivirus
{
    public class AntiVirusInventoryDto
    {
        public string DisplayName { get; set; }

        public string Provider { get; set; }

        public string RealtimeScanner { get; set; }

        public string DefinitionStatus { get; set; }

        public int ProductState { get; set; }
    }
}
