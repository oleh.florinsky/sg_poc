﻿namespace SessionGuardian.BLL.Models.Antivirus
{
    using System;

    public class AntiVirusWmiDto
    {
        public const string Query = "select * from AntiVirusProduct";

        public string DisplayName { get; set; }

        public UInt32 ProductState { get; set; }
    }
}
