﻿namespace SessionGuardian.BLL.Models.Defender
{
    public class BaseCommand
    {
        public string AdditionalMessage { get; set; }
        public BaseCommand(string additionalMessage)
        {
            AdditionalMessage = additionalMessage;
        }
    }
}
