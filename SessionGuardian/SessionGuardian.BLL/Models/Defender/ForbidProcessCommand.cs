﻿namespace SessionGuardian.BLL.Models.Defender
{
    public class ForbidProcessCommand : BaseCommand
    {
        public bool IgnoreRepetition { get; set; }

        public ForbidProcessCommand(string additionalMessage = "") : base(additionalMessage)
        {
        }
    }
}
