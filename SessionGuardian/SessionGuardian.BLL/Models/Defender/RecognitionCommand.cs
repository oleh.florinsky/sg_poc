﻿namespace SessionGuardian.BLL.Models.Defender
{
    using SessionGuardian.BLL.Enums;

    public class RecognitionCommand : BaseCommand
    {
        public RecognizerStatus Status { get; set; }
        public bool IgnoreRepetition { get; set; }

        public RecognitionCommand( RecognizerStatus status, bool ignoreRepetition, string additionalMessage = "") : base(additionalMessage)
        {
            Status = status;
            IgnoreRepetition = ignoreRepetition;
        }
    }
}
