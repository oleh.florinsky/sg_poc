﻿using System.Threading;
using System.Threading.Tasks;

namespace SessionGuardian.BLL.Interfaces
{
    public interface BaseVerificationInterface
    {
        Task UpdateActualState(bool successInviroments, CancellationToken token );
    }
}
