﻿namespace SessionGuardian.BLL.Interfaces.Luxand
{
    using SessionGuardian.BLL.Models.Recognition;
    using System;
    using System.Drawing;
    using System.Threading.Tasks;
    public interface ILuxandRecognitionInterface : IBaseRecognitionInterface
    {
        FaceDetectOutputModel DetectFaceAsync(IntPtr handle);

        Task<LabelDetectOutputModel> DetectLabelsAsync(Bitmap image);

        CompareFacesOutputModel FindAnchorFace();


    }
}
