﻿namespace SessionGuardian.BLL.Enums
{
    // server`s custom statuses
    public enum BadResponseStatus
    {
        FORBIDDEN = 4403,//
        NOT_FOUND = 4404, //
        SESSION_RESTARTED = 4405,//
        CONFIGURATION_NOT_ACTIVE = 4406,
        NOT_ALLOWED_IP = 4407,//
        CONFIGURATION_NOT_FOUND = 4408,
        //CONFIGURATION_NOT_ACTIVE = 4409//
    }
}
