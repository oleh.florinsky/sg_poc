﻿namespace SessionGuardian.BLL.Enums
{
    public enum SocketMessageType
    {

        NO_RESPONSE = 0,


        //wait response
        AUTHENTICATION = 1,
        GET_PROFILE_CONFIG = 2,
        GET_SECURITY_TOKENS = 3,
        GET_WATERMARK_CONFIG = 4,
        GET_WORKSPACE_DATA = 18,

        //success response
        AUTHENTICATION_SUCCESS = 5,
        GET_WATERMARK_CONFIG_SUCCESS = 6,
        GET_PROFILE_CONFIG_SUCCESS = 7,
        GET_SECURITY_TOKENS_SUCCESS = 14,


        //error response
        AUTHENTICATION_ERROR = 8,
        MESSAGE_PARSE_ERROR = 9,
        GET_PROFILE_CONFIG_ERROR = 10,
        GET_WATERMARK_CONFIG_ERROR = 11,
        GET_SECURITY_TOKENS_ERROR = 15,
        GET_WORKSPACE_DATA_SUCCESS = 19,


        // updated in runtime
        UPDATE_PROFILE_CONFIG_EVENT = 16,
        UPDATE_PROFILE_CONFIG_IMAGE_EVENT = 17,


        //without response
        KEEP_ALIVE = 12,
        LOG = 13,


    }
}
