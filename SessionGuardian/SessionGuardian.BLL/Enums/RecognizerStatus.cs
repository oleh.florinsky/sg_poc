﻿namespace SessionGuardian.BLL.Enums
{
    public enum RecognizerStatus
    {
        DEFAULT = 0,
        DETECTED_COINCIDENCE_FACE = 1,
        DETECTED_DIFFERENT_FACE = 2,
        DETECTED_MORE_FACES = 3,
        NOT_DETECT_FACE = 4,
        DETECTED_FORBIDDEN_DEVICE = 5,
        NOT_OPEN_EYES = 6,
    }
}
