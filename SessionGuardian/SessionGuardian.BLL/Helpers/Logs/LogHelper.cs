﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SessionGuardian.BLL.Helpers.Logs
{
    public static class LogHelper
    {
        static LogHelper()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }

        public static ILog GetLoggerRollingFileAppender(string logName, string fileName)
        {
            var log = LogManager.Exists(logName);

            if (log != null) return log;

            var appenderName = $"{logName}Appender";
            log = LogManager.GetLogger(logName);
            ((Logger)log.Logger).AddAppender(GetRollingFileAppender(appenderName, fileName));

            return log;
        }

        public static RollingFileAppender GetRollingFileAppender(string appenderName, string fileName)
        {
            var layout = new PatternLayout { ConversionPattern = "%date{dd.MM.yyyy HH:mm:ss.fff} [%thread] [%level]  %message%newline" };
            layout.ActivateOptions();

            var appender = new RollingFileAppender
            {
                Name = appenderName,
                File = fileName,
                AppendToFile = true,
                RollingStyle = RollingFileAppender.RollingMode.Size,
                MaxSizeRollBackups = 5,
                MaximumFileSize = "2MB",
                Layout = layout,
                StaticLogFileName = true,
                ImmediateFlush = true,
                LockingModel = new FileAppender.MinimalLock(),
                Encoding = Encoding.UTF8,
            };

            appender.ActivateOptions();

            return appender;
        }
    }
}
