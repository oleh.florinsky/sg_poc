﻿using Microsoft.Win32.SafeHandles;
using SessionGuardian.Common;
using System;
using System.Security;

namespace SessionGuardian.BLL.Helpers.UnmanagedCode
{
    public class SafeHBitmapHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        [SecurityCritical]
        public SafeHBitmapHandle(IntPtr preexistingHandle, bool ownsHandle)
            : base(ownsHandle)
        {
            SetHandle(preexistingHandle);
        }

        protected override bool ReleaseHandle()
        {
            return Win32Api.DeleteObject(handle);
        }
    }
}
