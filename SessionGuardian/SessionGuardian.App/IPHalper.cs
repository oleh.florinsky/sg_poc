﻿namespace SessionGuardian.App
{
    using SessionGuardian.BLL.Services.Logs;
    using System.Net;
    using System.Text.RegularExpressions;

    public class IPHalper
    {
        public static string GetExternalIP()
        {
            try
            {
                var webclient = new WebClient();
                var externalIP = webclient.DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch (System.Exception ex)
            {
                LogService.Info(ex.StackTrace);
                return "external IP not found...";
            }        
        }
    }
}
