﻿using Newtonsoft.Json;
using SessionGuardian.App.Windows;
using SessionGuardian.BLL.Enums;
using SessionGuardian.BLL.Models.Credential;
using SessionGuardian.BLL.Models.Sockets;
using SessionGuardian.BLL.Models.Sockets.Responce;
using SessionGuardian.BLL.Models.WaterMark;
using SessionGuardian.BLL.Services.Profile;
using SessionGuardian.BLL.Services.Secure;
using SessionGuardian.BLL.Services.WaterMark;
using SessionGuardian.BLL.Services.WebSockets;
using SessionGuardian.Common.Enums.WebSockets;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace SessionGuardian.App.Manager
{
    public sealed class WaterMarkManager
    {
        WaterMarkService waterMarkService;

        private static readonly object Instancelock = new object();
        private static WaterMarkManager instance = null;

        private string projectName;
        private string userId;
        private string externalIP;

        private WebSocketService webSocketService;

        public static WaterMarkManager GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new WaterMarkManager();
                        }
                    }
                }
                return instance;
            }
        }



        List<WaterMarkWindow> waterMarkList = new List<WaterMarkWindow>();


        private Timer _timer;

        // Time between now and when the timer was started last
        private TimeSpan _currentElapsedTime = TimeSpan.Zero;

        // The last time the timer was started
        private DateTime _startTime = DateTime.MinValue;



        public WaterMarkManager()
        {
            webSocketService = WebSocketService.GetInstance;
            waterMarkService = WaterMarkService.GetInstance;


            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                var waterMark = new WaterMarkWindow();
                waterMarkList.Add(waterMark);
            }

            projectName = ProfileService.GetInstance.Configuration.ProjectTitle;
            userId = AppConfiguration.GetInstance.UserName;
            externalIP = IPHalper.GetExternalIP();

            // Set up a timer and fire the Tick event once per second (1000 ms)
            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Tick += new EventHandler(_timer_Tick);
        }

        private void SetVisability(bool isVisible, WaterMarkConfigModel waterMarkconfigs = null)
        {
            foreach (var item in waterMarkList)
            {
                item.Dispatcher.Invoke(() =>
                {
                    if (isVisible)
                    {

                        if (waterMarkconfigs != null)
                        {
                            item.Notification.Opacity = waterMarkconfigs.Opacity;
                            var bc = new BrushConverter();
                            item.Notification.Foreground = (Brush)bc.ConvertFrom(waterMarkconfigs.Color);
                            item.Notification.FontWeight = waterMarkconfigs.fontWeight == "Normal" ? FontWeights.Normal : FontWeights.Bold;
                            item.Notification.FontSize = waterMarkconfigs.FontSize;
                            item.Notification.FontFamily = waterMarkconfigs.FontFamily != "SYSTEM_DEFAULT" ? new System.Windows.Media.FontFamily(waterMarkconfigs.FontFamily) : item.FontFamily;
                        }
                        item.Show();
                    }
                    else
                    {
                        item.Hide();
                    }
                });
            }
        }

        public void SetNotification(string time)
        {
            foreach (var item in waterMarkList)
            {
                item.Notification.Text = $"{projectName} - {userId} - {time} - {externalIP}";
            }
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            var timeSinceStartTime = DateTime.Now - _startTime;
            timeSinceStartTime = new TimeSpan(timeSinceStartTime.Hours, timeSinceStartTime.Minutes, timeSinceStartTime.Seconds);
            SetNotification(DateTime.Now.ToString(@"H:mm"));
        }


        public void Show()
        {
            _startTime = DateTime.Now;
            _timer.Start();
            SetVisability(true, waterMarkService.Configurations);
        }


        public void Hide()
        {
            _timer.Stop();
            SetVisability(false);
        }
    }
}
