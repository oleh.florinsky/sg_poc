﻿namespace SessionGuardian.App.Manager
{
    using SessionGuardian.Common;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;

    public class ProcessManager
    {
        public static void CheckOneInstance()
        {
            Process proc = Process.GetCurrentProcess();
            int count = Process.GetProcesses().Where(p =>
                p.ProcessName == proc.ProcessName).Count();


            //var processes = Process.GetProcesses().Where(p =>
            //p.ProcessName == proc.ProcessName);
            //foreach (var item in processes)
            //{
            //    item.Kill();
            //}

            if (count > 1)
            {
                MessageBox.Show("Already an instance of SessionGuardian is running...");
                proc.Kill();
            }
        }
        public static void ShutdownApp()
        {
            AppService.ShutdownApp();
        }

    }
}
