﻿namespace SessionGuardian.App.Manager
{
    using SessionGuardian.App.Windows;
    using SessionGuardian.BLL.Enums;
    using SessionGuardian.BLL.Models.Defender;
    using SessionGuardian.BLL.Models.Sockets;
    using SessionGuardian.BLL.Services.Guardians;
    using SessionGuardian.BLL.Services.Profile;
    using SessionGuardian.BLL.Services.ThirdParty;
    using SessionGuardian.BLL.Services.WebSockets;
    using SessionGuardian.Common;
    using SessionGuardian.Common.Enums.WebSockets;
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Threading;

    public class GuardManager
    {
        private SynchronizationContext syncContext;



        private bool currentSecureState;
        private int countdown = 30;
        DispatcherTimer _shutdownTimer;
        TimeSpan _shutdownTime;

        private static System.Threading.Timer _sendKeepAliveTimer;

        public static List<GuardWindow> guardWindows = new List<GuardWindow>();

        private WebSocketService webSocketService;

        private static CancellationTokenSource cancelTokenSource;
        CancellationToken token;

        private static readonly object Instancelock = new object();
        private ProfileService profileService;
        private Dictionary<Type, Action<BaseCommand>> stateCommandTypes;

        public delegate void ForcedStopSession();
        public event ForcedStopSession ForcedStopSessionHandler;

        private ProcessSnapShotService processSnapShotService;
        private CamDeviceService camDeviceService;
        private ProcessService processService;
        private WorkSpaceService workSpaceService;
        private KeyboardHookService keyboardHookService;
        private WaterMarkManager waterMarkManager;


        private static GuardManager instance = null;

        public delegate void ProcessRecognition(RecognitionCommand command);

        public static GuardManager GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new GuardManager();
                        }
                    }
                }
                return instance;
            }
        }


        public GuardManager()
        {
            syncContext = SynchronizationContext.Current;
            profileService = ProfileService.GetInstance;

            processService = ProcessService.GetInstance;
            processSnapShotService = ProcessSnapShotService.GetInstance;
            camDeviceService = CamDeviceService.GetInstance;
            workSpaceService = WorkSpaceService.GetInstance;
            webSocketService = WebSocketService.GetInstance;


            keyboardHookService = KeyboardHookService.GetInstance;
            keyboardHookService.SetHook();
            keyboardHookService.OnKeyDownEvent += kh_OnKeyDownEvent;


            waterMarkManager = WaterMarkManager.GetInstance;

            _shutdownTimer = new DispatcherTimer();
            _shutdownTimer.Tick += new EventHandler(timer_Tick);
            _shutdownTimer.Interval = new TimeSpan(0, 0, 1);

            webSocketService.ReceivedAwsTokenHandler += processSnapShotService._awsService.UpdateCredentials;

            stateCommandTypes = new Dictionary<Type, Action<BaseCommand>> {
                { typeof(RecognitionCommand), (x) =>
                    {
                        RecognitionCommandHandle((RecognitionCommand) x);
                    }
                },
                { typeof(CamDeviceCommand), (x) =>
                    {
                        CamDeviceCommandHandle((CamDeviceCommand) x);
                    }
                },
                 { typeof(ForbidProcessCommand), (x) =>
                    {
                        ForbidProcessCommandHandle((ForbidProcessCommand) x);
                    }
                }
            };
            //for (int i = 0; i < Screen.AllScreens.Length; i++)
            //{
            //    var guardWindow = new GuardWindow();
            //    guardWindow.Show();

            //    guardWindows.Add(guardWindow);
            //}

            var guardWindow = new GuardWindow();
            guardWindow.Show();
            guardWindows.Add(guardWindow);
        }


        private void kh_OnKeyDownEvent(object sender, KeyEventArgs e)
        {
            Console.WriteLine("?????????????????????????????????????????????????????????????????<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>");
        }



        private void timer_Tick(object sender, EventArgs e)
        {
            Console.WriteLine($"********************************************************************************************************************Tick {_shutdownTime}");

            if (_shutdownTime == TimeSpan.Zero)
            {
                _shutdownTimer.Stop();



                ForcedStopSessionHandler?.Invoke();
                //DisableGuardians();
                _shutdownTime = TimeSpan.FromSeconds(countdown); // reset timer
                syncContext.Send(state =>
                {
                    BlockKeyBoardAndMouse(false);
                }, null);

                currentSecureState = false;

                foreach (var window in guardWindows) // set the default notification after forced stop session
                {
                    window.Dispatcher.Invoke(() =>
                    {
                        window.UpdateShutDownContent(_shutdownTime.Seconds);
                    });
                }


            }
            else
            {
                foreach (var window in guardWindows)
                {
                    window.Dispatcher.Invoke(() =>
                    {
                        window.UpdateShutDownContent(_shutdownTime.Seconds);
                    });
                }
                _shutdownTime = _shutdownTime.Add(TimeSpan.FromSeconds(-1));
            }
        }


        public async void StartGuardSession()
        {
            foreach (var item in guardWindows)
            {
                item.Visibility = Visibility.Visible;
                item.NotificationMessage.Content = "loading...";
            }

            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;

            workSpaceService.ShutDownRequiredApps();
            EnableGuardians();
        }


        private async void EnableGuardians()
        {
            // enable waterMark
            waterMarkManager.Show();




            var webCamTask = Task.Run(() =>
           {
               camDeviceService.ActivateSubscription<BaseCommand>(BaseStateHandle);
               camDeviceService.StartMonitoring();
           });

            processService.ActivateSubscription<BaseCommand>(BaseStateHandle);
            processService.StartMonitoring();





            // start processing  the snapshot from webcam stream
            var snapShothandlerTask = Task.Run(() =>
           {
               processSnapShotService.ActivateSubscription<BaseCommand>(BaseStateHandle);

               processSnapShotService.StartHandleSnapshot(token);
           });



            var keepAliveTask = Task.Run(() =>
           {
               _sendKeepAliveTimer = new System.Threading.Timer(SendKeepAlive, null, (profileService.Configuration.KeepAliveTimeout - 1) * 1000, Timeout.Infinite);
           });

            workSpaceService.Start();
            await Task.WhenAll(webCamTask, snapShothandlerTask, keepAliveTask);
        }

        public void DisableGuardians()
        {
            // stop processing snapshots from webcam device
            cancelTokenSource.Cancel();

            // stop signals of observers
            processSnapShotService.RecognitionStateChange.OnCompleted();
            processService.ProcessChange.OnCompleted();
            camDeviceService.WebCamStateChange.OnCompleted();


            // stop webcam
            camDeviceService.StopMonitoring();

            // stop processes monitoring
            processService.StopMonitoring();

            // hide protected windows
            foreach (var item in guardWindows)
            {
                item.Dispatcher.Invoke(() =>
                {
                    item.Visibility = Visibility.Hidden;
                });
            }

            workSpaceService.ShutDownRequiredApps();
            // hide waterMark
            waterMarkManager.Hide();

            _sendKeepAliveTimer?.Change(Timeout.Infinite, Timeout.Infinite);

            webSocketService.CloseConnection();
        }


        private async void SendKeepAlive(Object state)
        {
            await webSocketService.SendMessage<object>(SocketMessageType.KEEP_ALIVE, "keepalive", token);
            _sendKeepAliveTimer.Change(profileService.Configuration.KeepAliveTimeout * 1000, Timeout.Infinite);


            //var keepAliveMessage = new RequestMessageModel()
            //{
            //    Type = SocketMessageType.KEEP_ALIVE.ToString(),
            //    Payload = null
            //};
            //var keepAliveRequest = webSocketService.CreateRequestMessage(keepAliveMessage, "keepalive");
            //webSocketService.SendMessage(SocketRequestType.SendMessage, keepAliveRequest);
        }



        public void BaseStateHandle(BaseCommand recognitionState)
        {
            stateCommandTypes[recognitionState.GetType()](recognitionState);
        }

        private void ForbidProcessCommandHandle(ForbidProcessCommand processCommand)
        {
            ShowDefender(processCommand.AdditionalMessage);
        }

        public void CamDeviceCommandHandle(CamDeviceCommand camDeviceCommand)
        {
            ShowDefender(camDeviceCommand.AdditionalMessage);
        }

        public void RecognitionCommandHandle(RecognitionCommand recognitionCommand)
        {
            switch (recognitionCommand.Status)
            {
                case RecognizerStatus.DETECTED_COINCIDENCE_FACE:
                    HideDefender();
                    break;
                case RecognizerStatus.DETECTED_DIFFERENT_FACE:
                    ShowDefender(recognitionCommand.AdditionalMessage);
                    break;
                case RecognizerStatus.NOT_DETECT_FACE:
                    ShowDefender(recognitionCommand.AdditionalMessage);
                    break;
                case RecognizerStatus.NOT_OPEN_EYES:
                    ShowDefender(recognitionCommand.AdditionalMessage);
                    break;
                case RecognizerStatus.DETECTED_MORE_FACES:
                    ShowDefender(recognitionCommand.AdditionalMessage);
                    break;
                case RecognizerStatus.DETECTED_FORBIDDEN_DEVICE:
                    if (currentSecureState == false)
                    {
                        ShowDefender(recognitionCommand.AdditionalMessage);
                    }
                    else
                    {
                        UpdateNotificationMessage(recognitionCommand.AdditionalMessage);
                    }
                    break;
            }
        }

        private void HideDefender()
        {
            currentSecureState = false;
            if (_shutdownTimer.IsEnabled)
            {
                _shutdownTimer.Stop();
            }

            foreach (var window in guardWindows)
            {
                window.Dispatcher.Invoke(() =>
                 {
                     window.NotificationMessage.Content = "";
                     window.Hiden();
                 });
            }

            syncContext.Send(state =>
            {
                BlockKeyBoardAndMouse(false);
            }, null);
        }


        private void UpdateNotificationMessage(string message)
        {
            foreach (var window in guardWindows)
            {
                window.Dispatcher.Invoke(() =>
                {
                    window.NotificationMessage.Content = message;
                    //window.Show();
                });
            }
        }


        private void ShowDefender(string message)
        {
            _shutdownTime = TimeSpan.FromSeconds(countdown); // reset timer
            currentSecureState = true;

            if (!_shutdownTimer.IsEnabled)
            {
                _shutdownTimer.Start();
                Console.WriteLine($"********************************************************************************************************************Timer start");
            }

            foreach (var window in guardWindows)
            {
                window.Dispatcher.Invoke(() =>
               {
                   //_shutdownTime = TimeSpan.FromSeconds(countdown);
                   window.UpdateShutDownContent(_shutdownTime.Seconds);
                   window.NotificationMessage.Content = message;
                   window.Display();
               });
            }

            syncContext.Send(state =>
            {
                BlockKeyBoardAndMouse(true);
            }, null);
        }

        public void BlockKeyBoardAndMouse(bool state)
        {
            var res = Win32Api.BlockInput(state);
        }
    }
}
