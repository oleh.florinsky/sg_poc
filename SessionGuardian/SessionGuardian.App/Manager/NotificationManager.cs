﻿namespace SessionGuardian.App.Manager
{
    using SessionGuardian.App.Windows;
    using SessionGuardian.BLL.Services.Error;
    using System;
    using System.Threading.Tasks;
    using System.Windows;

    public class NotificationManager
    {
        ErrorService errorService;

        public delegate void CriticalStopSession();
        public event CriticalStopSession CriticalStopSessionHandler;

        public NotificationManager()
        {
            errorService = ErrorService.GetInstance;
        }

        public void StartNotificationMonitoring()
        {
            errorService.CriticalNotificationHandler += HandelCriticalEvent;
        }

        private async void HandelCriticalEvent(string text, bool showPopup)
        {
            Console.WriteLine($"Critical event - {text}");

            CriticalStopSessionHandler.Invoke();

            if (showPopup)
            {
                errorService.CriticalNotificationHandler -= HandelCriticalEvent;
            
               await Application.Current.Dispatcher.InvokeAsync((Action)delegate
                {
                    NotificationPopup popup = new NotificationPopup();
                    popup.SetNotificationMessage(text);
                    popup.ShowDialog();
                });
            }
        }
    }
}
