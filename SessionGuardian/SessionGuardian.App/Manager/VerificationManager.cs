﻿namespace SessionGuardian.App.Manager
{
    using FontAwesome.WPF;
    using SessionGuardian.App.Windows;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Models.Verification;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.Verifications;
    using SessionGuardian.BLL.Services.WebSockets;
    using SessionGuardian.Common.Enums.UI;
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    public class VerificationManager
    {
        VerificationWindow verificationWindow;
        private WebSocketService webSocketService;

        private NotificationManager notificationManager;
        private GuardManager guardManager;
        private volatile SRServerService srServerService;

        Brush WaitingBrush = Brushes.Silver;
        Brush SuccessBrush = (Brush)(new BrushConverter().ConvertFrom("#40963b"));
        Brush FailureBrush = (Brush)(new BrushConverter().ConvertFrom("#d1423b"));

        private Timer showSpinnerTimer;
        List<ImageAwesome> verificationIconList;


        public VerificationManager()
        {
            //var waterMark = new WaterMarkWindow();
            //waterMark.Show();
            webSocketService = WebSocketService.GetInstance;




            verificationWindow = new VerificationWindow();
            verificationWindow.Show();
            SetDefaultControlState();

            var window = verificationWindow;

            verificationIconList = new List<ImageAwesome>() { window.SRServerIcon, window.UserProfileIcon, window.AntivirusIcon, window.OSIcon, window.SGSecurityIcon, window.SGSecurityIcon, window.CameraIcon, window.VMIcon };

            verificationWindow.UpdateVerificationHandler += HandleStartVerification;
            verificationWindow.StartSecureSessionHandler += HandleStartSession;
            verificationWindow.StopSecureSessionHandler += HandleStopSessison;

            webSocketService.ReceivedServerMessageHandler += ShowReceivedMessageSpinner;

            notificationManager = new NotificationManager();

            notificationManager.CriticalStopSessionHandler += HandleCriticalNotification;

            notificationManager.StartNotificationMonitoring();

        }


        public void SetDefaultControlState()
        {
            SetWaitingState(verificationWindow.ForwardSwitherIcon);

            SwitchButtonState(verificationWindow.SessionStartButton, false);

            verificationWindow.SessionStartButton.MouseEnter += (s, e) => Mouse.OverrideCursor = Cursors.Hand;
            verificationWindow.SessionStopButton.MouseEnter += (s, e) => Mouse.OverrideCursor = Cursors.Hand;
            verificationWindow.SessionStartButton.MouseLeave += (s, e) => Mouse.OverrideCursor = Cursors.Arrow;
            verificationWindow.SessionStopButton.MouseLeave += (s, e) => Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void SwitchButtonState(Button button, bool state)
        {
            verificationWindow.Dispatcher.Invoke(() =>
            {

                if (state)
                {
                    button.IsEnabled = true;
                    button.Background = (Brush)(new BrushConverter().ConvertFrom("#2976b1"));
                }
                else
                {
                    button.IsEnabled = false;
                    button.Background = (Brush)(new BrushConverter().ConvertFrom("#c1c1c2"));
                }
            });
        }



        public async void HandleStartVerification()
        {
            verificationWindow.ExternalIPLable.Content = IPHalper.GetExternalIP();

            var verificationState = await UpdateVerificationState();
            SetStateAfterDoneVerification(verificationState);
        }


        private void SetStateAfterDoneVerification(bool isSuccess)
        {
            if (isSuccess)
            {
                SetGreenState(verificationWindow.ForwardSwitherIcon);
                SwitchButtonState(verificationWindow.SessionStartButton, true);
            }
            else
            {
                SetRedState(verificationWindow.ForwardSwitherIcon);
                SwitchButtonState(verificationWindow.SessionStartButton, false);
            }
            SwitchButtonState(verificationWindow.RefreshButton, true);
        }

        public async void HandleStartSession()
        {
            guardManager = GuardManager.GetInstance;
            var verificationState = await UpdateVerificationState();
            SetStateAfterDoneVerification(verificationState);
            if (verificationState)
            {
                guardManager.ForcedStopSessionHandler += HandleForcedStopSession;
                guardManager.StartGuardSession();

                SwitchButtionVisability(verificationWindow.SessionStartButton);
                SwitchButtionVisability(verificationWindow.SessionStopButton);
            }
        }


        private void HandleCriticalNotification()
        {
            Console.WriteLine("CRITICAL STOP GUARDIAN SESSION");
            HandleStopSessison();
        }

        private void HandleForcedStopSession()
        {
            Console.WriteLine("FORCED STOP GUARDIAN SESSION");
            HandleStopSessison();
        }


        private void HandleStopSessison()
        {
            if (guardManager != null)
            {
                guardManager.DisableGuardians();
                guardManager.ForcedStopSessionHandler -= HandleForcedStopSession;
                SwitchButtionVisability(verificationWindow.SessionStartButton);
                SwitchButtionVisability(verificationWindow.SessionStopButton);
            }
        }

        public async Task<bool> UpdateVerificationState()
        {
            try
            {
                foreach (var item in verificationIconList)
                {
                    SetForegroundColor(item, WaitingBrush);
                }

                //var frame = new Mat();
                //var capture = new VideoCapture(0);
                //capture.Open(0);

                //if (capture.IsOpened())
                //{


                //        capture.Read(frame);
                //        var image = BitmapConverter.ToBitmap(frame);
                //        //if (pictureBox1.Image != null)
                //        //{
                //        //    pictureBox1.Image.Dispose();
                //        //}
                //        //pictureBox1.Image = image;

                //}

                bool generalVerificationState = false;
                await Task.Run(async () =>
                {

                    SwitchButtonState(verificationWindow.SessionStartButton, false);
                    SetWaitingState(verificationWindow.ForwardSwitherIcon);
                    SwitchButtonState(verificationWindow.RefreshButton, false);

                    var verificationStates = new List<bool>();

                    var internetService = new InternetService();
                    var res = await SetActualVerificationState(internetService, verificationWindow.ICMarkerIcon, 15000);
                    verificationStates.Add(res);
                    LogService.Info($"Internet verification - {internetService.actualState}", true, nameof(App));

                    srServerService = srServerService ?? new SRServerService();

                    verificationStates.Add(await SetActualVerificationState(srServerService, verificationWindow.SRServerIcon, isSuccessInviroments: internetService.actualState));
                    LogService.Info($"Server connection verification - {srServerService.actualState}", true, nameof(App));

                    verificationWindow.Dispatcher.Invoke(() =>
                    {
                        verificationWindow.UserNameLable.Content = srServerService.actualState ? AppConfiguration.GetInstance.UserName : "";
                    });

                    var userProfileService = new UserProfileService();
                    verificationStates.Add(await SetActualVerificationState(userProfileService, verificationWindow.UserProfileIcon, 5000, isSuccessInviroments: srServerService.actualState));
                    LogService.Info($"Profile verification - {userProfileService.actualState}", true, nameof(App));

                    var antivirusService = new AntivirusService();
                    verificationStates.Add(await SetActualVerificationState(antivirusService, verificationWindow.AntivirusIcon, 2000));
                    LogService.Info($"AV verification - {antivirusService.actualState}", true, nameof(App));

                    var oSService = new OSService();
                    verificationStates.Add(await SetActualVerificationState(oSService, verificationWindow.OSIcon, 2000));
                    LogService.Info($"OS verification - {oSService.actualState}", true, nameof(App));

                    var sGSecurityService = new SGSecurityService();
                    verificationStates.Add(await SetActualVerificationState(sGSecurityService, verificationWindow.SGSecurityIcon, 10000, isSuccessInviroments: userProfileService.actualState));
                    LogService.Info($"Security verification - {sGSecurityService.actualState}", true, nameof(App));

                    var cameraService = new CameraService();
                    verificationStates.Add(await SetActualVerificationState(cameraService, verificationWindow.CameraIcon, 2000));
                    LogService.Info($"WebCamera verification - {cameraService.actualState}", true, nameof(App));

                    var vmService = new VMService();
                    verificationStates.Add(await SetActualVerificationState(vmService, verificationWindow.VMIcon, 2000, isSuccessInviroments: userProfileService.actualState));
                    LogService.Info($"VirtualMachine verification - {vmService.actualState}", true, nameof(App));



                    generalVerificationState = verificationStates.FindAll(x => x == false).Count == 0 ? true : false;
                    Console.WriteLine($"@@@@@@@@@@@ GENERAL VERIFICATION STATE IS - {generalVerificationState } @@@@@@@@@@@");
                });
                return generalVerificationState;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async Task<bool> SetActualVerificationState(BaseVerification verificationItem, ImageAwesome marker, int waitingTime = 10000, bool isSuccessInviroments = true)
        {
            Console.WriteLine($"###START Verification \"{verificationItem.GetType().Name}\" ");
            SetWaitingState(marker);

            var verificationCancelTokenSource = new CancellationTokenSource(waitingTime);
            var token = verificationCancelTokenSource.Token;
            await verificationItem.UpdateActualState(isSuccessInviroments, token);


            if (verificationItem.actualState)
            {
                SetGreenState(marker);
            }
            else
            {
                SetRedState(marker);
            }
            Console.WriteLine($"###END Verification \"{verificationItem.GetType().Name}\" result: {verificationItem.actualState} ");
            Console.WriteLine($"");
            return verificationItem.actualState;
        }


        private void ShowReceivedMessageSpinner()
        {
            Task.Run(() =>
            {
                verificationWindow.Dispatcher.Invoke(() =>
                {
                    SetGridVisability(verificationWindow.UpdateConfigPanel, true);

                    showSpinnerTimer = new Timer((x) => HideSpinner(x), null, 500, Timeout.Infinite);

                });
            }).ConfigureAwait(false);
        }

        private void HideSpinner(Object state)
        {
            verificationWindow.Dispatcher.Invoke(() =>
            {
                SetGridVisability(verificationWindow.UpdateConfigPanel, false);
            });
            //showSpinnerTimer.
        }




        private void SetWaitingState(ImageAwesome marker)
        {

            verificationWindow.Dispatcher.Invoke(() =>
            {
                marker.Spin = true;
                marker.Foreground = WaitingBrush;
                marker.Icon = FontAwesomeIcon.CircleOutlineNotch;
            });
            Console.WriteLine($"Show waiting marker ------------------------------------------");
            Thread.Sleep(200); // short delay for spinner
        }

        private void SetForegroundColor(ImageAwesome marker, Brush color)
        {
            marker.Foreground = color;
        }

        private void SetGreenState(ImageAwesome marker)
        {
            verificationWindow.Dispatcher.Invoke(() =>
            {
                marker.Spin = false;
                marker.Foreground = SuccessBrush;
                marker.Icon = FontAwesomeIcon.CheckCircleOutline;
            });
        }

        private void SetRedState(ImageAwesome marker)
        {
            verificationWindow.Dispatcher.Invoke(() =>
            {
                marker.Spin = false;
                marker.Foreground = FailureBrush;
                marker.Icon = FontAwesomeIcon.TimesCircleOutline;
            });
        }

        private void SwitchButtionVisability(Button button1)
        {
            Task.Run(() =>
            {
                verificationWindow.Dispatcher.Invoke(() =>
                {
                    button1.Visibility = button1.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                });
            });
        }

        private void SetGridVisability(Grid grid, bool visabilityState)
        {
            //Console.WriteLine($"GET MESSAGE FROM SERVER++++++++++++++++++++++++++++++++++++++++++++++++{visabilityState}");
            grid.Visibility = visabilityState ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }
    }
}
