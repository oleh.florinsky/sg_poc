﻿using SessionGuardian.BLL.Services.Logs;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace SessionGuardian.App
{
    [RunInstaller(true)]
    public partial class SessionGuardianInstaller : System.Configuration.Install.Installer
    {

        public SessionGuardianInstaller()
        {
            InitializeComponent();
        }

        public override void Uninstall(IDictionary savedState)
        {
            if (savedState != null)
            {
                base.Uninstall(savedState);
            }
            string targetDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            try
            {
                base.Uninstall(savedState);

                if (Directory.Exists(targetDir))
                {
                    Directory.Delete(targetDir, true);
                }
              
            }
            catch (Exception ex)
            {
                LogService.Info(ex.StackTrace);
            }
        }
    }
}
