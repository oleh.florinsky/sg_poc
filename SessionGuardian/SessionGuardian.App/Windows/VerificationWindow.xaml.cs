﻿namespace SessionGuardian.App.Windows
{
    using SessionGuardian.App.Manager;
    using SessionGuardian.Common.Extensions;
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for VerificationWindow.xaml
    /// </summary>
    public partial class VerificationWindow : Window
    {
        public delegate void UpdateVerificationInfo();
        public event UpdateVerificationInfo UpdateVerificationHandler;

        public delegate void StartSecureSession();
        public event StartSecureSession StartSecureSessionHandler;

        public delegate void StopSecureSession();
        public event StopSecureSession StopSecureSessionHandler;



        public VerificationWindow()
        {
            InitializeComponent();
            this.BuildVersionLable.Content = $"v. {Assembly.GetExecutingAssembly().GetName().Version.ToString()}";
        }

        private void CheckAgainButton_Click(object sender, RoutedEventArgs e)
        {
            //RefreshButton.IsEnabled = false;
            UpdateVerificationState();
        }

        private void UpdateVerificationState()
        {
            UpdateVerificationHandler?.Invoke();
        }



        private void SessionStartButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("USER START GUARDIAN SESSION");
            StartSecureSessionHandler?.Invoke();
        }
        private void SessionStopButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("USER STOP GUARDIAN SESSION");
            StopSecureSessionHandler?.Invoke();
        }





        private void SwitchPage_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            string code = button.Tag.ToString();
            if (code.ToBoolean())
            {
                MainPage.Visibility = Visibility.Collapsed;
                VerificationPage.Visibility = Visibility.Visible;
            }
            else
            {
                MainPage.Visibility = Visibility.Visible;
                VerificationPage.Visibility = Visibility.Collapsed;
            }
        }

        private void App_Closing(Object sender, CancelEventArgs e)
        {

            foreach (var process in Process.GetProcessesByName("workspaces"))
            {
                process.Kill();
            }
            ProcessManager.ShutdownApp();
        }
        //public void SetDefaultControlState()
        //{

        //    ForwardSwitherIcon.Spin = true;
        //    ForwardSwitherIcon.Foreground = SuccessBrush;
        //    ForwardSwitherIcon.Icon = FontAwesomeIcon.CheckCircleOutline;
        //}
    }
}
