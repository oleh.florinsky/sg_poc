﻿namespace SessionGuardian.App.Windows
{
    using SessionGuardian.Common;
    using SessionGuardian.Common.Enums.UI;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Interop;


    public partial class GuardWindow : Window
    {
        Screen screen;
        public static int numberScreen = 0;


        public GuardWindow()
        {
            InitializeComponent();



        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            screen = Screen.AllScreens[numberScreen];
            Rectangle rectangle = screen.WorkingArea;
            Left = rectangle.Left;
            WindowState = WindowState.Maximized;
            numberScreen++;
            EnableBlur();
        }


        internal void EnableBlur()
        {
            var windowHelper = new WindowInteropHelper(this);
            var accent = new AccentPolicy();
            accent.AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND;

            var accentStructSize = Marshal.SizeOf(accent);
            var accentPtr = Marshal.AllocHGlobal(accentStructSize);

            Marshal.StructureToPtr(accent, accentPtr, false);

            var data = new WindowCompositionAttributeData();

            data.Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY;
            data.SizeOfData = accentStructSize;
            data.Data = accentPtr;

            Win32Api.SetWindowCompositionAttribute(windowHelper.Handle, ref data);

            Marshal.FreeHGlobal(accentPtr);
        }


        public void Hiden()
        {
            this.Visibility = Visibility.Hidden;
        }

        public void Display()
        {
            Topmost = true;
            this.Visibility = Visibility.Visible;
        }

        public void UpdateShutDownContent(int downCount)
        {
            ShutDownMessage.Content = $"Shutdown occurred in {downCount} sec.";
        }
    }
}
