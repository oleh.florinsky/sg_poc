﻿using SessionGuardian.App.Manager;
using System.Windows;
using System.Windows.Shell;

namespace SessionGuardian.App.Windows
{
    /// <summary>
    /// Interaction logic for NotificationPopup.xaml
    /// </summary>
    public partial class NotificationPopup : Window
    {
        public NotificationPopup()
        {
            InitializeComponent();
        }

        public void Loaded_Popup(object sender, RoutedEventArgs e)
        {
            // delete white border line on top of wrapper window 
            WindowChrome windowChrome = new WindowChrome();
            windowChrome.GlassFrameThickness = new Thickness(0);
            windowChrome.CornerRadius = new CornerRadius(0);
            windowChrome.CaptionHeight = 0;
            WindowChrome.SetWindowChrome(this, windowChrome);
        }

        public void SetNotificationMessage(string message)
        {
            TextBoxNotification.Text = $"ERROR: {message}";
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
            ProcessManager.ShutdownApp();
        }
    }
}
