﻿namespace SessionGuardian.App.Windows
{
    using SessionGuardian.App.Manager;
    using System;
    using System.ComponentModel;
    using System.Windows;

    /// <summary>
    /// Interaction logic for WelcomeWindow.xaml
    /// </summary>
    public partial class WelcomeWindow : Window
    {
        public WelcomeWindow()
        {
            InitializeComponent();
        }

        private void App_Closing(Object sender, CancelEventArgs e)
        {
            ProcessManager.ShutdownApp();
        }
    }
}
