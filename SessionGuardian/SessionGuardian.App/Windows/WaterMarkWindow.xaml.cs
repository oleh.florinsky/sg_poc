﻿namespace SessionGuardian.App.Windows
{
    using SessionGuardian.Common;
    using System;
    using System.Drawing;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Interop;

    /// <summary>
    /// Interaction logic for WaterMarkWindow.xaml
    /// </summary>
    public partial class WaterMarkWindow : Window
    {
        public static int numberScreen = 0;

        public WaterMarkWindow()
        {
           
            InitializeComponent();

            Screen screen = Screen.AllScreens[numberScreen];
            Rectangle rectangle = screen.WorkingArea;
            Top = rectangle.Top;
            Left = rectangle.Left;
            numberScreen++;

        }

        public const Int32 WM_NCHITTEST = 0x84;
        public const Int32 HTTRANSPARENT = -1;


        void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;



            #region Hide the window from Taskbar and windows switcher(Alt+Tab)
            ShowInTaskbar = false;
            WindowInteropHelper wndHelper = new WindowInteropHelper(this);
            int exStyle = (int)Win32Api.GetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE);
            exStyle |= (int)ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            Win32Api.SetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);
            #endregion

            //HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            //source.AddHook(new HwndSourceHook(WndProc));
        }

        //IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        //{
        //    if (msg == (int)WM_NCHITTEST)
        //    {
        //        handled = true;
        //        return (IntPtr)HTTRANSPARENT;
        //    }

        //    return IntPtr.Zero;
        //}

        protected override void OnSourceInitialized(EventArgs e)
        {

            base.OnSourceInitialized(e);
            var hwnd = new WindowInteropHelper(this).Handle;
            WindowsServices.SetWindowExTransparent(hwnd);
        }
    }


    public static class WindowsServices
    {
        const int WS_EX_TRANSPARENT = 0x00000020;
        const int GWL_EXSTYLE = (-20);



        public static void SetWindowExTransparent(IntPtr hwnd)
        {
            var extendedStyle = Win32Api.GetWindowLong(hwnd, GWL_EXSTYLE);
            Win32Api.SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_TRANSPARENT);
        }
    }
}
