﻿namespace SessionGuardian.App
{
    using AForge.Video.DirectShow;
    using Luxand;
    using SessionGuardian.App.Manager;
    using SessionGuardian.App.Windows;
    using SessionGuardian.BLL.Interfaces.Luxand;
    using SessionGuardian.BLL.Models.Credential;
    using SessionGuardian.BLL.Services.Logs;
    using SessionGuardian.BLL.Services.LuxandRecognition;
    using SessionGuardian.BLL.Services.Protocol;
    using SessionGuardian.BLL.Services.Secure;
    using System;
    using System.Drawing;
    using System.IO;
    using System.Windows;
    //using System.Windows.Forms;

    public partial class App : Application
    {
        String key = "vKH7eS8BnDHySF7/JnAuIL2ntNetFq2BAPHcCXMlL4JMTxMNEfrK7fKKGR7yDnYOSaEE25LKjhHQmr2l/MN2L3/LSyOtd3tNrnN65hnWsMQawzEBZrYMMWT8weaAt6zlvKeIS+D6CVh0S5bPB+xZUx4ylUYSKMd53jGMXUqKIoA=";
        int anchorImageHandler = 0;
        int trackerHandler = 0;
        long anchorID = 0;
        protected override void OnStartup(StartupEventArgs e)
        {
            ProcessManager.CheckOneInstance();
            //SchemeCredentialService.RegisterMyProtocol();


            //int state = FSDKCam.InitializeCapturing();
            //int sdk = FSDK.ActivateLibrary("vKH7eS8BnDHySF7/JnAuIL2ntNetFq2BAPHcCXMlL4JMTxMNEfrK7fKKGR7yDnYOSaEE25LKjhHQmr2l/MN2L3/LSyOtd3tNrnN65hnWsMQawzEBZrYMMWT8weaAt6zlvKeIS+D6CVh0S5bPB+xZUx4ylUYSKMd53jGMXUqKIoA=");
            //FSDK.InitializeLibrary();

            //var trackerConfig = $"DetectExpression=true; Threshold={0.992}; HandleArbitraryRotations=true; DetermineFaceRotationAngle=false; InternalResizeWidth={100}; FaceDetectionThreshold={5};";
            //if (FSDK.FSDKE_OK != FSDK.ActivateLibrary(key))
            //{
            //    //MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)", "Error activating FaceSDK", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //}

            //FSDK.InitializeLibrary();
            //FSDKCam.InitializeCapturing();
            //string anchorPath = Path.GetDirectoryName(typeof(LuxandRecognitionService).Assembly.Location) + $"\\Resource\\Photos\\face1.png";

            ////Load an anchor image
            //if (FSDK.FSDKE_OK != FSDK.LoadImageFromFileW(ref anchorImageHandler, anchorPath))
            //{
            //    //MessageBox.Show("Please select a propper anchor image", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    //Application.Exit();
            //}
            //var webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            //var cam = new VideoCaptureDevice(webcam[0].MonikerString);
            //FSDK.CreateTracker(ref trackerHandler);

            //var errorHandler = 0;
            //FSDK.SetTrackerMultipleParameters(trackerHandler, trackerConfig, ref errorHandler);

            //long[] ids = { };
            //long faceCount = 0;

            ////Add timeout
            //while (faceCount != 1)
            //{
            //    FSDK.FeedFrame(trackerHandler, 1, anchorImageHandler, ref faceCount, out ids, sizeof(long) * 256);
            //}

            //Array.Resize(ref ids, (int)faceCount);
            //anchorID = ids[0];
            //cam.NewFrame += (o, args) =>
            //{
            //    Bitmap bit = (Bitmap)args.Frame.Clone();

            //    Image i = (Image)bit;
            //    var result = FSDK.LoadImageFromCLRImage(ref anchorImageHandler, i);

            //    FSDK.CImage image = new FSDK.CImage(bit.GetHbitmap());
            //    //Image frameImage = image.ToCLRImage();
            //    //Graphics gr = Graphics.FromImage(frameImage);

            //    // make UI controls accessible (to find if the user clicked on a face)
            //    //Application.DoEvents();

        

            //    FSDK.FeedFrame(trackerHandler, 0, image.ImageHandle, ref faceCount, out ids, sizeof(long) * 256); // maximum of 256 faces detected



            //    Console.WriteLine($"New frame {faceCount}");
            //};
            ////cam.PlayingFinished += Cam_PlayingFinished;
            //cam.Start();

            LogService.InfoStart();
            try
            {
                var existCredential = false;
                if (e.Args.Length > 0)
                {
                    LogService.Info($"START FROM URL", true, nameof(App));
                    var protocolCredential = SchemeCredentialService.GetCredentialFromLink(e.Args[0]);
                    LogService.Info($"SgToken: {e.Args[0]}", false, nameof(SchemeCredentialService));
                    var userConfiguration = new UserConfiguration()
                    {
                        JwtToken = protocolCredential.JwtToken,
                        //AWSRegCode = protocolCredential.AWSRegCode,
                        //AWSUser = protocolCredential.AWSUser,
                        SgEndpoint = protocolCredential.SgEndpoint
                        //SgToken = protocolCredential.SgToken
                    };
                    
                    //LogService.Info($"SgToken: {userConfiguration.SgToken}", false, nameof(SchemeCredentialService));
                    //LogService.Info($"AWSRegCode: {userConfiguration.AWSRegCode}", false, nameof(SchemeCredentialService));
                    //LogService.Info($"AWSUser: {userConfiguration.AWSUser}", false, nameof(SchemeCredentialService));
                    LogService.Info($"SgEndpoint: {userConfiguration.SgEndpoint}", false, nameof(SchemeCredentialService));
                    CredentialService.SaveCredential(userConfiguration);
                }
                else
                {

                    LogService.Info("START FROM EXE", true, nameof(App));

                    if (!CredentialService.HasCredential)
                    {
                        var welcomeWindow = new WelcomeWindow();
                        welcomeWindow.Show();
                        existCredential = true;
                    }

                    //var userConfiguration = new UserConfiguration()
                    //{
                    //    AWSRegCode = "SLiad+ACPRZS",
                    //    AWSUser = "Joe.Remote2",
                    //    SgEndpoint = "ec2-18-232-83-73.compute-1.amazonaws.com:8080/ws-connection",
                    //    SgToken = "5db6d880-926f-11e9-b42c-61570412aaa8"
                    //};

                    //var userConfiguration = new UserConfiguration()
                    //{
                    //    AWSRegCode = "SLiad+ACPRZS",
                    //    AWSUser = "Joe.Remote",
                    //    SgEndpoint = "ec2-18-232-83-73.compute-1.amazonaws.com:8080/ws-connection",
                    //    SgToken = "8575edbe-82d1-11e9-bc42-526af7764f64"
                    //};

                    //CredentialService.SaveCredential(userConfiguration);

                }
                if (!existCredential)
                {
                    var verificationManager = new VerificationManager();
                    verificationManager.HandleStartVerification();
                  
                }
            }
            catch (Exception ex)
            {
                LogService.Error(ex.Message, true, nameof(App));
                throw;
            }
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);


        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Console.WriteLine("MyHandler caught : " + e.Message);
            Console.WriteLine("Runtime terminating: {0}", args.IsTerminating);
        }
    }
}
