﻿namespace SessionGuardian.Common.Models
{
    public class RequestModel
    {
        public byte[] Message { get; set; }

        public string MessageTypeAlias { get; set; }
    }
}
