﻿namespace SessionGuardian.Common
{
    using System;

    public class AppService
    {
        public static void ShutdownApp()
        {
            Environment.Exit(0);
        }
    }
}
