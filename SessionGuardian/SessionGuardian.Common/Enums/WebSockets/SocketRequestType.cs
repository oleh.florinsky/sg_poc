﻿
namespace SessionGuardian.Common.Enums.WebSockets
{
    public enum SocketRequestType
    {
        SendMessage = 1,
        CloseOutputConnection = 3
    }
}
