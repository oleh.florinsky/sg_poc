﻿using System.Runtime.InteropServices;

namespace SessionGuardian.Common.Enums.UI
{
    [StructLayout(LayoutKind.Sequential)]
    public struct AccentPolicy
    {
        public AccentState AccentState;
        public int AccentFlags;
        public int GradientColor;
        public int AnimationId;
    }
}
